@extends('admin.admin_layouts')
@section('title', 'Category Create')
@section('admin_content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title  text-center m-t-0 m-b-30"> Update Brand</h4>
                <form method="post" action="{{ route('brand.update', $brand->id) }}" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $brand->name }}">
                    </div>

                    <div class="form-group">
                        <label for="filer_input">Logo</label>
                        <input type="file" name="logo" class="form-control" id="filer_input">
                        <img class="" src="{{ URL::to($brand->logo) }}" style="width:30px; height: 30px;">
                        <input  type="hidden" name="old_logo" value="{{$brand->logo}}">
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                </form>
            </div>
        </div><!-- end col -->
    </div>
@endsection


@section('on_page_js')
    <script>
        $(document).ready(function() {
            $('#filer_input').filer( {
                    limit: 1,
                    maxSize: 3,
                    extensions: ["jpg", "png", "gif"],
                    showThumbs: true
                }
            );
        });
    </script>

@endsection
