@extends('admin.admin_layouts')
@section('title', 'Admin Login')


@section('admin_content')

    <div class="text-center logo-alt-box">
        <a href="{{ route('admin.home') }}" class="logo"><span>Modi<span>Ponno</span></span></a>
    </div>

    <div class="wrapper-page">
    <div class=" card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
        </div>
        <div class="panel-body">
            <form action="{{ route('admin_login.store') }}" method='post'>
                @csrf
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email Address">
                    </div>
                </div>
                @error('email')
                <span class="invalid-feedback" role="alert">
                       <strong>{{ $message }}</strong>
                   </span>
                @enderror
                <br/>
                <br/>
                <br/>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                    </div>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                @enderror
                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-custom">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light text-uppercase" type="submit">Log In</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="{{ route('admin.password.request') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                    </div>
                </div>


{{--                <div class="form-group m-t-20 m-b-0">--}}
{{--                    <div class="col-sm-12 text-center"><h4>Sign In with</h4></div>--}}
{{--                </div>--}}

{{--                <div class="form-group m-b-0 text-center">--}}
{{--                    <div class="col-sm-12">--}}
{{--                        <button type="button" class="btn btn-facebook waves-effect waves-light m-t-20"><i--}}
{{--                                class="fa fa-facebook m-r-5"></i> Facebook--}}
{{--                        </button>--}}
{{--                        <button type="button" class="btn btn-twitter waves-effect waves-light m-t-20"><i--}}
{{--                                class="fa fa-twitter m-r-5"></i> Twitter--}}
{{--                        </button>--}}
{{--                        <button type="button" class="btn btn-googleplus waves-effect waves-light m-t-20"><i--}}
{{--                                class="fa fa-google-plus m-r-5"></i> Google+--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}

            </form>

        </div>
    </div>
    <!-- end card-box -->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">Don't have an account? <a href="!#" class="text-primary m-l-5"><b>Sign Up</b></a></p>
        </div>
    </div>

</div>
<!-- end wrapper page -->
@endsection
