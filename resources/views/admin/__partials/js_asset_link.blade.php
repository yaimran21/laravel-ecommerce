<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ asset('backend/js/jquery.min.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
<script src="{{ assert('backend/js/detect.js') }}"></script>
<script src="{{ asset('backend/js/fastclick.js') }}"></script>
<script src="{{ asset('backend/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('backend/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('backend/js/waves.js') }}"></script>
<script src="{{ asset('backend/js/wow.min.js') }}"></script>
<script src="{{ asset('backend/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('backend/js/jquery.scrollTo.min.js') }}"></script>



<!-- Datatables -->
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/jszip.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/pdfmake.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/vfs_fonts.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/buttons.print.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/responsive.bootstrap.min.js') }}"></script>

<!-- Datatable init js -->
<script src=" {{ asset('backend/pages/jquery.datatables.init.js') }}"></script>

{{--<!-- Tag input js -->--}}
{{--<script src="{{asset('backend/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>--}}

{{--<!-- isotope plugin js -->--}}
{{--<script src="{{ asset('backend/plugins/isotope/js/isotope.pkgd.min.js') }}"></script>--}}

{{--<script src="{{ asset('backend/js/jquery.core.js') }}"></script>--}}
{{--<script src="{{ asset('backend/js/jquery.app.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="{{ asset('https://unpkg.com/sweetalert/dist/sweetalert.min.js')}}"></script>

{{--<script src="{{ asset('backend/plugins/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>--}}
{{--<script type="text/javascript" src="{{ asset('backend/plugins/multiselect/js/jquery.multi-select.js') }}"></script>--}}

{{--<script src="{{ assert('backend/plugins/jquery.filer/js/jquery.filer.min.js') }}"></script>--}}



{{--<script type="text/javascript">--}}
{{--    $(window).load(function(){--}}
{{--        var $container = $('.masonry-container');--}}
{{--        $container.isotope({--}}
{{--            filter: '*'--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}

<!-- Data table -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable( { keys: true } );
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable( { ajax: "backend/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    TableManageButtons.init();
</script>




<!-- Jquery filer js -->
<script src="{{ asset('backend/plugins/jquery.filer/js/jquery.filer.min.js') }}"></script>
<!-- App js -->
<script src="{{ asset('backend/js/jquery.core.js') }}"></script>
<script src="{{ asset('backend/js/jquery.app.js') }}"></script>

<!-- page specific js -->
<script src="{{ asset('backend/pages/jquery.fileuploads.init.js') }}"></script>


<script src="{{ asset('backend/plugins/summernote/summernote.min.js') }}"></script>
<script src="{{ asset('backend/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
<script src="{{ asset('backend/plugins/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>


<script>
    @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>

@yield('on_page_js')

