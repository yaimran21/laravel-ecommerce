@extends('admin.admin_layouts')
@section('title', 'Product index')
@section('admin_content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <h4 style="display: inline-block" class="header-title m-t-0 m-b-30">Product Index</h4>
                <a href="{{ route('product.create') }}">
                    <button   type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light "> Create New</button>

                </a>


                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                    <tr>
                        <th>Id#</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Categories</th>
                        <th>Brand</th>
                        <th>Sku</th>
                        <th>Price</th>
                        <th>SalePrice</th>
                        <th>Status</th>
                        <th> Attribute</th>
                        <th>ImageGallery</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($products as $product)
                        <?php $i++; ?>
                        <tr>
                            <td>{{$i}}</td>
                            <td><img src="{{url('media/products/large',$product->image)}}" alt="" width="25" height="25"></td>
                            <td class="text-center">{{ $product->title }}</td>
                            <td class="text-center">
                                @foreach( $product->categories as $category)
                                    <li> {{ isset($category->name) ? $category->name: 'Product has no category' }}</li>
                                @endforeach
                            </td>
                            <td class="text-center">{{ is_object($product->brand)? $product->brand->name: '--' }}</td>
                            <td class="text-center">{{ $product->sku }}</td>
                            <td class="text-center">{{ $product->price }}</td>
                            <td class="text-center">{{ $product->sale_price }}</td>
                            <td class="text-center">{{ $product->status == 1? 'Published': 'Unpublished' }}</td>
                            <td class="text-center">
                                <a href="{{route('attribute.show',$product->id)}}" class="btn btn-success btn-xs">Add Attribute</a>
                            </td>
                            <td class="text-center">
                                <a  href="{{route('imagegallery.show',$product->id)}}" class="btn btn-primary btn-xs" >Add Images</a>
                            </td>

                            <td class="text-center">
                                <a class="btn btn-primary btn-xs" href="{{ route('product.edit', $product->id) }} ">
                                    <i class="fa fa-edit">
                                    </i>
                                </a>

                                <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="{{ $product->id }}" data-toggle="modal" data-target=".product_delete_modal">
                                    <i id="product_title" data-id="{{ $product->title }}" class="fa fa-trash">
                                    </i>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


{{--    <div id="coupon-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">--}}
{{--        <div class="modal-dialog">--}}
{{--            <form method="post" action="{{ route('coupon.store') }}">--}}
{{--                @csrf--}}
{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header">--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
{{--                        <h4 class="modal-title"> Create coupon</h4>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="name">Coupon code</label>--}}
{{--                            <input type="text" name="code" class="form-control" id="categoryName" placeholder="Enter unique coupon code">--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <label for="type"> Coupon Type</label>--}}
{{--                            <select id="type" class="form-control" name="type"  onchange="changeFunc();">--}}
{{--                                <option class="text-muted" value="" >--Select Coupon Type--</option>--}}
{{--                                <option value="1">Fixed</option>--}}
{{--                                <option value="2">Percent</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <input style="display: none" type="text" name="value" class="form-control"  id="fixedValue" placeholder="Enter fixed discount value">--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <input style="display: none" type="text" name="percent_of" class="form-control"  id="percentOf" placeholder="Enter percent value">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="status">Status</label>--}}
{{--                            <select class="form-control" name="status" id="status" required>--}}
{{--                                <option class="text-muted" value="" >--Select Status--</option>--}}
{{--                                <option value="1">Active</option>--}}
{{--                                <option value="2">Inactive</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                        <button type="submit" class="btn btn-info">Save</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}


{{--        </div>--}}
{{--    </div><!-- /.modal -->--}}


    <!-- Delete Modal -->



    <div class="modal fade product_delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="mySmallModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="product_title">
                    <form id="deleteForm" action="" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('on_page_js')


    <script>
        function changeFunc() {
            var selectBox = document.getElementById("type");
            var selectedValue = selectBox.options[selectBox.selectedIndex].value;
            if (selectedValue=="1"){
                $('#fixedValue').show();
                $('#percentOf').hide();
            }
            else if (selectedValue=="2") {
                $('#percentOf').show();
                $('#fixedValue').hide();

            }
            else {
                alert("Select a valid option");
                $('#percentOf').hide();
                $('#fixedValue').hide();            }
        }
    </script>
    <script>
        $(document).on("click", ".delete-modal", function () {
            let productId = $(this).data('id');
            let product_title = $('#product_title').data('id');
            $(".modal-body #product_title").text( "Do you want to delete "+ product_title +' coupon?' );
            var route = '/admin/product/'+productId
            $('#deleteForm').attr('action', route)
        });
    </script>

{{--    <script>--}}
{{--        $(document).ready(function() {--}}
{{--            $('#datatable').dataTable();--}}
{{--            $('#datatable-keytable').DataTable( { keys: true } );--}}
{{--            $('#datatable-responsive').DataTable();--}}
{{--            $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );--}}
{{--            var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );--}}
{{--        } );--}}
{{--        TableManageButtons.init();--}}
{{--    </script>--}}
@endsection
