<?php

namespace App\Http\Controllers;
use App\Order;
use Illuminate\Http\Request;
use Cart;
use Auth;

class OrderController extends Controller
{
    public function order(Request $request){
        $input_data=$request->all();
        $payment_method=$input_data['payment_method'];
        $input_data['grand_total'] = Cart::total();;
//        dd($input_data);
        Order::create($input_data);

        if($payment_method=="COD"){
            return redirect()->route('checkout.cod');
        }else{
            return redirect('/paypal');
        }
    }
    public function cod(){
        $user_order=Order::where('user_id',Auth::id())->first();
        Cart::destroy();
        return view('frontend.payment.cod',compact('user_order'));
    }
    public function paypal(Request $request){
        $who_buying=Order::where('user_id',Auth::id())->first();
        return view('payment.paypal',compact('who_buying'));
    }
}
