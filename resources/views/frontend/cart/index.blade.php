@extends('layouts.app')
@section('title', 'কার্ট')

@section('home_content')
    <!-- Start content -->
    <div class="container">
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">My Cart</h4>
                    <table class="table table table-hover m-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Product Title</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cart_items as $c_iteam)
                            <?php
                            $i++; ?>
                            <tr>
                                <th scope="row">{{ $i }}</th>
                                <td><img style="height: 30px; width: 30px;" src="{{ url('media/products/small',$c_iteam->options->image) }}" alt=""></td>
                                <td>{{ $c_iteam->name }}</td>
                                {{--                                <td>{{ $c_iteam->qty }}</td>--}}
                                <td>
                                    <form action="{{ route('cart.update')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="rowId" value="{{ $c_iteam->rowId }}">
                                        <input type="number" name="qty" value="{{$c_iteam->qty}}" style="width: 50px;">
                                        <button type="submit" class="btn-success btn-sm"><i class="fa fa-check-square"></i></button>
                                    </form>

                                </td>
                                <td>{{ $c_iteam->price }}</td>
                                <td>{{ $c_iteam->price*$c_iteam->qty }}</td>
                                <td>
                                    {{--                                    <a class="" href=" ">--}}
                                    {{--                                        <i class="fa fa-edit" style="color: blue">--}}
                                    {{--                                        </i>--}}
                                    {{--                                    </a>--}}
                                    <a class="" href="{{ route('cart.remove', $c_iteam->rowId) }}">
                                        <i class="fa fa-trash" style="color: red">
                                        </i>
                                    </a>

                                    {{--                                    <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="" data-toggle="modal" data-target=".product_delete_modal">--}}
                                    {{--                                        <i id="product_title" data-id="" class="fa fa-trash">--}}
                                    {{--                                        </i>--}}
                                    {{--                                    </a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <table class="table table m-0">
                        <thead>
                        <th></th>
                        </thead>

                        <tbody>
                        <td align="right">Total: <b>{{ Cart::total() }}</b> Tk</td>

                        </tbody>
                    </table>
                    <table class="table table m-0">
                        <thead>
                        <th></th>
                        </thead>

                        <tbody>
                        <td align="right">
                            <a class="" href="{{ route('checkout') }}">
                                <button class="btn btn btn-success">Checkout</button>
                            </a>
                        </td>

                        </tbody>
                    </table>


                </div>
            </div><!-- end col -->
        </div>            </div> <!-- container -->
@endsection
