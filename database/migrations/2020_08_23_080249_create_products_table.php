<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('brand_id')->index();
            $table->string('title');
            $table->text('details')->nullable();
            $table->string('sku');
            $table->unsignedInteger('quantity')->nullable();
            $table->decimal('price', 8, 2);
            $table->decimal('sale_price', 8, 2)->nullable();
            $table->decimal('weight', 8, 2)->nullable();
            $table->string('color')->nullable();
            $table->string('video_link')->nullable();
            $table->string('image')->nullable();
            $table->integer('status')->default(1);
//
            $table->timestamps();
//
//            // foreign key assign
//            $table->foreign('category_id')->references('id')->
//            on('categories')->onDelete('cascade');
//
//            $table->foreign('sub_category_id')->references('id')->
//            on('sub_categories');
//
            $table->foreign('brand_id')->references('id')->on('brands');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
