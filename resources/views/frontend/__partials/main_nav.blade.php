<header class="xara-header xara-header--half-tranparent">
    {{--    <div class="topbar-area">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-sm-5 col-xs-12 text-xs-center">--}}
    {{--                    <div class="pdb10 pdt5">--}}
    {{--                        Get big sale offer. <span class="base-color">80%</span> discount--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-sm-7 col-xs-12">--}}
    {{--                    <div class="text-right">--}}
    {{--                        <ul class="topbar-right">--}}
    {{--                            <li class="topbar-right__item">--}}
    {{--                                    <span class="topbar-right__icon">--}}
    {{--                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"--}}
    {{--                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">--}}
    {{--                                        <g>--}}
    {{--                                        	<g>--}}
    {{--                                        		<path d="M256,0C156.748,0,76,80.748,76,180c0,33.534,9.289,66.26,26.869,94.652l142.885,230.257--}}
    {{--                                        			c2.737,4.411,7.559,7.091,12.745,7.091c0.04,0,0.079,0,0.119,0c5.231-0.041,10.063-2.804,12.75-7.292L410.611,272.22--}}
    {{--                                        			C427.221,244.428,436,212.539,436,180C436,80.748,355.252,0,256,0z M384.866,256.818L258.272,468.186l-129.905-209.34--}}
    {{--                                        			C113.734,235.214,105.8,207.95,105.8,180c0-82.71,67.49-150.2,150.2-150.2S406.1,97.29,406.1,180--}}
    {{--                                        			C406.1,207.121,398.689,233.688,384.866,256.818z"/>--}}
    {{--                                        	</g>--}}
    {{--                                        </g>--}}
    {{--                                        <g>--}}
    {{--                                        	<g>--}}
    {{--                                        		<path d="M256,90c-49.626,0-90,40.374-90,90c0,49.309,39.717,90,90,90c50.903,0,90-41.233,90-90C346,130.374,305.626,90,256,90z--}}
    {{--                                        			 M256,240.2c-33.257,0-60.2-27.033-60.2-60.2c0-33.084,27.116-60.2,60.2-60.2s60.1,27.116,60.1,60.2--}}
    {{--                                        			C316.1,212.683,289.784,240.2,256,240.2z"/>--}}
    {{--                                        	</g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></g><g></g><g></g><g>--}}
    {{--                                        </svg>--}}

    {{--                                    </span>--}}
    {{--                                <span class="topbar-right__text">Store Locator</span>--}}
    {{--                            </li>--}}
    {{--                            <li class="topbar-right__item">--}}
    {{--                                    <span class="topbar-right__icon">--}}
    {{--                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"--}}
    {{--                                             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">--}}
    {{--                                             <g><g>--}}
    {{--                                        		<path d="M437.02,330.98c-27.883-27.882-61.071-48.523-97.281-61.018C378.521,243.251,404,198.548,404,148--}}
    {{--                                        			C404,66.393,337.607,0,256,0S108,66.393,108,148c0,50.548,25.479,95.251,64.262,121.962--}}
    {{--                                        			c-36.21,12.495-69.398,33.136-97.281,61.018C26.629,379.333,0,443.62,0,512h40c0-119.103,96.897-216,216-216s216,96.897,216,216--}}
    {{--                                        			h40C512,443.62,485.371,379.333,437.02,330.98z M256,256c-59.551,0-108-48.448-108-108S196.449,40,256,40--}}
    {{--                                        			c59.551,0,108,48.448,108,108S315.551,256,256,256z"/>--}}
    {{--                                    	        </g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>--}}
    {{--                                        </svg>--}}

    {{--                                    </span>--}}
    {{--                                <span class="topbar-right__text">My Account</span>--}}
    {{--                            </li>--}}
    {{--                            <li class="topbar-right__item">--}}
    {{--                                <select class="currency-select">--}}
    {{--                                    <option value="1">USD</option>--}}
    {{--                                    <option value="2">TAKA</option>--}}
    {{--                                    <option value="3">EURO</option>--}}
    {{--                                </select>--}}
    {{--                            </li>--}}
    {{--                        </ul>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}


    <div class="main-menu-area-two">
        <div class="container">
            <div class="menu-logo">
                <div class="logo">
                    <a href="{{url('/')}}" class="logo-index"><img src="{{ asset('frontend/images/organic/logo.png') }}" alt="" /></a>
                </div>
                <nav id="easy-menu">
                    <ul class="menu-list">
                        <li><a class="bangla-font" href="{{ url('/') }}">হোম</a>
                            {{--                            <ul class="dropdown">--}}
                            {{--                                <li><a href="index.html">Home One</a></li>--}}
                            {{--                                <li><a href="index-electronics.html">Home Two</a></li>--}}
                            {{--                                <li><a href="index-fashion.html">Home Three</a></li>--}}
                            {{--                                <li><a href="index-furniture.html">Home Four</a></li>--}}
                            {{--                                <li><a href="index-organic.html">Home Five</a></li>--}}
                            {{--                            </ul>--}}
                        </li>
                        <li><a class="bangla-font" href="shop.html"> শপ</a>
                        <li><a class="bangla-font" href="shop.html"> মধু</a>
                        <li><a class="bangla-font" href="shop.html"> চাপাতা</a>
                            {{--                            <div class="dropdown-mega-menu column-3">--}}
                            {{--                                <ul>--}}
                            {{--                                    <li><span class="dropdown-mega-menu__title">shop style </span> </li>--}}
                            {{--                                    <li><a href="shop-sidebar.html">Shop Sidebar</a></li>--}}
                            {{--                                    <li><a href="shop-banner.html">Shop Banner</a></li>--}}
                            {{--                                    <li><a href="shop.html">Shop Fullwidth</a></li>--}}
                            {{--                                    <li><a href="#">Shop Category</a></li>--}}
                            {{--                                    <li><a href="#">Shop 3 Column</a></li>--}}
                            {{--                                    <li><a href="#">Shop 4 Column</a></li>--}}
                            {{--                                    <li><a href="#">Shop 5 Column</a></li>--}}
                            {{--                                </ul>--}}
                            {{--                                <ul>--}}
                            {{--                                    <li><span class="dropdown-mega-menu__title">product type</span> </li>--}}
                            {{--                                    <li><a href="#">Example One</a></li>--}}
                            {{--                                    <li><a href="#">Example Two</a></li>--}}
                            {{--                                    <li><a href="#">Example Three</a></li>--}}
                            {{--                                    <li><a href="#">Standard Product</a></li>--}}
                            {{--                                    <li><a href="#">Variable Product</a></li>--}}
                            {{--                                    <li><a href="#">Grouped Product</a></li>--}}
                            {{--                                    <li><a href="#">Affiliate Product</a></li>--}}
                            {{--                                </ul>--}}
                            {{--                                <ul>--}}
                            {{--                                    <li><span class="dropdown-mega-menu__title">SHOP OPTION</span> </li>--}}
                            {{--                                    <li><a href="cart.html">Shopping Cart</a></li>--}}
                            {{--                                    <li><a href="#">Checkout</a></li>--}}
                            {{--                                    <li><a href="#">My Account</a></li>--}}
                            {{--                                    <li><a href="#">Order Tracking</a></li>--}}
                            {{--                                </ul>--}}
                            {{--                                <img class="megamenu-image" src="images/organic/menu-image.jpg" alt="">--}}
                            {{--                            </div>--}}
                        </li>
                        {{--                        <li><a href="cart.html">pages</a></li>--}}
                        {{--                        <li><a href="single-product.html">elements</a>--}}
                        {{--                            <ul class="dropdown">--}}
                        {{--                                <li><a href="single-product.html">Single Product</a></li>--}}
                        {{--                                <li><a href="single-product2.html">Single Product 2</a></li>--}}
                        {{--                            </ul>--}}
                        {{--                        </li>--}}
                        <li><a class="bangla-font" href="blog.html">ব্লগ</a>
                            <ul class="dropdown">
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="blog-details.html">Blog Details</a></li>

                            </ul>
                        </li>
                    </ul>
                </nav><!--#easy-menu-->
                <div class="header-icons">
                    <div class="search-box">
                        <a href="#!" class="base-color search-box__link">
                            <i class="icon-icomoon-magnifying-glass"></i>
                        </a>
                        <div class="top-search-input-wrap">
                            <span class="close-icon"><i class="icon-icomoon-close"></i></span>
                            <div class="top-search-overlay"></div>
                            <form action="#" method="post">
                                <div class="search-wrap">
                                    <div class="search  pull-right educon-top-search">
                                        <div class="sp_search_input">
                                            <input name="searchword" maxlength="200" class="pull-right" placeholder="Search" type="text"/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!--/.search-box -->
                    <div class="favorite-box">
                        <?php
                        $user_id = Auth::id();
                        $wishlist_count = DB::table('wish_lists')->where('user_id', $user_id)->count();
                        ?>
                        <a href="#" class="favorite-box_link"><i class="icon-icomoon-heart"></i>
                            <span class="base-gradient-bg count-number">{{$wishlist_count}}</span> </a>
                    </div>
                    <div class="cart-box">
                        <a href="{{ route('cart.index') }}" class="cart-box__link"><i class="icon-icomoon-shopping-cart2"></i> <span class="base-gradient-bg count-number">{{ Cart::count()
                     }}</span> </a>
                    </div>
                    {{--                    <div class="cart-box">--}}
                    {{--                        <a href="#" class="cart-box__link"><i class="glyphicon glyphicon-user"></i></a>--}}
                    {{--                    </div>--}}
                    <a href="#" class="hidden-lg hidden-md header-humberger-icon" id="humbarger-icon"><i class="fa fa-bars"></i> </a>
                    <div class="cart-box">

                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <br>
                                        <a class="dropdown-item" href="{{ route('logout') }}">
                                            WishList
                                        </a>


                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                    {{--                    @if(Auth::check())--}}
                    {{--                        <div class="cart-box">--}}
                    {{--                            <a href="#" class=""><i class="fa fa-user fa-5x"> Profile</i></a>--}}
                    {{--                            --}}
                    {{--                        </div>--}}

                    {{--                    @else--}}
                    {{--                        <div class="cart-box">--}}
                    {{--                            <a href="{{route('login')}}" class=""><i class="fa fa-user fa-5x"> Profile</i></a>--}}
                    {{--                            <a href="{{ route('register') }}" class=""><i class="fa fa-user fa-5x"> Profile</i></a>--}}
                    {{--                        </div>--}}
                    {{--                    @endif--}}

                </div>
            </div>
        </div>
    </div>
</header>
<nav class="mobile-background-nav">
    <div class="mobile-inner">
        <span class="mobile-menu-close"><i class="icon-icomoon-close"></i></span>
        <ul class="menu-accordion">
            <li><a href="index.html" class="bangla-font"> হোম</a>
            </li>
            <li><a class="bangla-font" href="shop.html"> শপ</a> </li>
            <li><a class="bangla-font has-submenu" href="!#"> ক্যাটেগরি <i class="fa fa-angle-down"></i></a>

                <ul class="dropdown">
                    <li><a class="bangla-font" href="shop.html"> মধু</a> </li>
                    <li><a class="bangla-font" href="shop.html"> চাপাতা</a> </li>
                </ul>
            </li>
            <li><a href="blog.html" class=" bangla-font">ব্লগ</a>
            <li><a class="bangla-font" href="cart.html"> কার্ট</a></li>
        </ul>
    </div>
</nav>



<nav class="mobile-category-nav">
    <div class="mobile-inner">
        <span class="mobile-menu-close"><i class="icon-icomoon-close"></i></span>
        <ul class="menu-accordion">
            <li><a href="index.html" class="bangla-font"> হোম</a>
            </li>
            <li><a class="bangla-font" href="shop.html"> শপ</a> </li>
            <li><a class="bangla-font has-submenu" href="!#"> ক্যাটেগরি <i class="fa fa-angle-down"></i></a>

                <ul class="dropdown">
                    <li><a class="bangla-font" href="shop.html"> মধু</a> </li>
                    <li><a class="bangla-font" href="shop.html"> চাপাতা</a> </li>
                </ul>
            </li>
            <li><a href="blog.html" class=" bangla-font">ব্লগ</a>
        </ul>
    </div>
</nav>

