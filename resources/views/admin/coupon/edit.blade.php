@extends('admin.admin_layouts')
@section('title', 'Category Create')
@section('admin_content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title  text-center m-t-0 m-b-30"> Update Brand</h4>
                <form method="post" action="{{ route('coupon.update', $coupon->id) }}">
                    @csrf
                    @method('PUT')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="code">Coupon Code</label>
                        <input type="text" name="code" class="form-control" id="code" value="{{ $coupon->code }}" disabled >
                    </div>
                    <div class="form-group">
                        <label for="type">Coupon Type</label>
                        <input type="text" name="type" class="form-control" id="type" value="{{ $coupon->type==1? 'Fixed': 'Percent' }}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="value">Value</label>
                        <input type="number" step="0.01" name="value" class="form-control" id="value" value="{{ $coupon->value }}" @if($coupon->type == '2') disabled @endif >
                    </div>

                    <div class="form-group">
                        <label for="percent_of">Percent Of</label>
                        <input type="number" step="0.01" name="percent_of" class="form-control" id="percent_of" value="{{ $coupon->percent_of }}" @if($coupon->type == '1') disabled @endif>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option class="text-muted" value="" >--Select Status--</option>
                            <option value="1" @if($coupon->status ==1) selected @endif>Active</option>
                            <option value="2" @if($coupon->status ==2) selected @endif>Inactive</option>
                        </select>

                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div><!-- end col -->
    </div>
@endsection
