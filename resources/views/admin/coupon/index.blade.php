@extends('admin.admin_layouts')
@section('title', 'Coupon index')
@section('admin_content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <h4 style="display: inline-block" class="header-title m-t-0 m-b-30">Coupon Index</h4>
                <button   type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light " data-toggle="modal" data-target="#coupon-create-modal"> Create New</button>


                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Code</th>
                        <th>Type</th>
                        <th>Value</th>
                        <th>Percent Of</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($coupons as $coupon)
                        <tr>
                            <td>{{ $coupon->id }}</td>
                            <td>{{ $coupon->code }}</td>
                            <td>{{ $coupon->type==1? 'Fixed': 'Percent' }}</td>
                            <td>{{ $coupon->value }}</td>
                            <td>{{ $coupon->percent_of }}</td>
                            <td>{{ $coupon->status==1? 'Active': 'Inactive' }}</td>

                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('coupon.edit', $coupon->id) }} ">
                                    <i class="fa fa-edit">
                                    </i>
                                </a>

                                <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="{{ $coupon->id }}" data-toggle="modal" data-target=".coupon_delete_modal">
                                    <i id="coupon_code" data-id="{{ $coupon->code }}" class="fa fa-trash">
                                    </i>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <div id="coupon-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form method="post" action="{{ route('coupon.store') }}">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"> Create coupon</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Coupon code</label>
                            <input type="text" name="code" class="form-control" id="categoryName" placeholder="Enter unique coupon code">
                        </div>

                        <div class="form-group">
                            <label for="type"> Coupon Type</label>
                            <select id="type" class="form-control" name="type"  onchange="changeFunc();">
                                <option class="text-muted" value="" >--Select Coupon Type--</option>
                                <option value="1">Fixed</option>
                                <option value="2">Percent</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input style="display: none" type="text" name="value" class="form-control"  id="fixedValue" placeholder="Enter fixed discount value">
                        </div>

                        <div class="form-group">
                            <input style="display: none" type="text" name="percent_of" class="form-control"  id="percentOf" placeholder="Enter percent value">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" name="status" id="status" required>
                                <option class="text-muted" value="" >--Select Status--</option>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </div>
            </form>


        </div>
    </div><!-- /.modal -->


    <!-- Delete Modal -->



    <div class="modal fade coupon_delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="mySmallModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="coupon_code">
                    <form id="deleteForm" action="" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('on_page_js')


    <script>
        function changeFunc() {
            var selectBox = document.getElementById("type");
            var selectedValue = selectBox.options[selectBox.selectedIndex].value;
            if (selectedValue=="1"){
                $('#fixedValue').show();
                $('#percentOf').hide();
            }
            else if (selectedValue=="2") {
                $('#percentOf').show();
                $('#fixedValue').hide();

            }
            else {
                alert("Select a valid option");
                $('#percentOf').hide();
                $('#fixedValue').hide();            }
        }
    </script>

    <script>
        $(document).on("click", ".delete-modal", function () {
            let couponId = $(this).data('id');
            let coupon_code = $('#coupon_code').data('id');
            $(".modal-body #coupon_code").text( "Do you want to delete "+ coupon_code +' coupon?' );
            var route = '/admin/coupon/'+couponId
            $('#deleteForm').attr('action', route)
        });

    </script>
@endsection
