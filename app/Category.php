<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'parent_id', 'logo', 'description', 'status'];

    public function products()
    {
//        dd($this->belongsToMany(Product::class)->withTimestamps());
        return $this->belongsToMany(Product::class)->withTimestamps()->limit(2);
//        return $this->belongsToMany(Product::class)->using(CategoryProduct::class);
    }
}
