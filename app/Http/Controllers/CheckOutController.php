<?php

namespace App\Http\Controllers;
use App\UserProfile;
use App\User;
use Auth;
use DB;
use Cart;
use Illuminate\Http\Request;

class CheckOutController extends Controller
{
    public function checkout(){
        if (Auth::check()){
            $user_info=UserProfile::where('user_id',Auth::id())->first();
            return view('frontend.checkout.index',compact('user_info'));
        }
        else {
            $notification=array(
                'message'=>"Login first to checkout please",
                'alert-type'=>'warning'
            );
            return redirect()->route('login')->with($notification);
        }


//        if (Auth::check()){
//            $notification=array(
//                'message'=>"Checkout successfully",
//                'alert-type'=>'success'
//            );
//            return redirect()->back()->with($notification);
//        }
//        else{
//            $notification=array(
//                'message'=>"Login first to checkout please",
//                'alert-type'=>'warning'
//            );
//            return redirect()->route('login')->with($notification);
//        }
    }



//    public function store(Request $request){
//        $this->validate($request,[
//            'billing_name'=>'required',
//            'billing_address'=>'required',
//            'billing_city'=>'required',
//            'billing_state'=>'required',
//            'billing_zipcode'=>'required',
//            'billing_mobile'=>'required',
//            'shipping_name'=>'required',
//            'shipping_address'=>'required',
//            'shipping_city'=>'required',
//            'shipping_state'=>'required',
//            'shipping_zipcode'=>'required',
//            'shipping_mobile'=>'required',
//        ]);
//        $input_data=$request->all();
//        $count_shippingaddress=DB::table('delivery_address')->where('users_id',Auth::id())->count();
//        if($count_shippingaddress==1){
//            DB::table('delivery_address')->where('users_id',Auth::id())->update([
//                'name'=>$input_data['shipping_name'],
//                'address'=>$input_data['shipping_address'],
//                'city'=>$input_data['shipping_city'],
//                'state'=>$input_data['shipping_state'],
//                'country'=>$input_data['shipping_country'],
//                'pincode'=>$input_data['shipping_pincode'],
//                'mobile'=>$input_data['shipping_mobile']]);
//        }else{
//            DB::table('delivery_address')->insert(['users_id'=>Auth::id(),
//                'users_email'=>Session::get('frontSession'),
//                'name'=>$input_data['shipping_name'],
//                'address'=>$input_data['shipping_address'],
//                'city'=>$input_data['shipping_city'],
//                'state'=>$input_data['shipping_state'],
//                'country'=>$input_data['shipping_country'],
//                'pincode'=>$input_data['shipping_pincode'],
//                'mobile'=>$input_data['shipping_mobile'],]);
//        }
//        DB::table('users')->where('id',Auth::id())->update(['name'=>$input_data['billing_name'],
//            'address'=>$input_data['billing_address'],
//            'city'=>$input_data['billing_city'],
//            'state'=>$input_data['billing_state'],
//            'country'=>$input_data['billing_country'],
//            'pincode'=>$input_data['billing_pincode'],
//            'mobile'=>$input_data['billing_mobile']]);
//        return redirect('/order-review');
//
//    }

    public function store(Request $request){
        $this->validate($request,[
            'billing_name'=>'required',
            'billing_address'=>'required',
            'billing_city'=>'required',
            'billing_state'=>'required',
            'billing_zipcode'=>'required',
            'billing_mobile'=>'required',
            'shipping_name'=>'required',
            'shipping_address'=>'required',
            'shipping_city'=>'required',
            'shipping_state'=>'required',
            'shipping_zipcode'=>'required',
            'shipping_mobile'=>'required',
        ]);
        $input_data=$request->all();
            DB::table('delivery_address')->where('user_id',Auth::id())->updateOrInsert([
                'user_id'=>Auth::id(),
                'user_name'=>$input_data['shipping_name'],
                'user_email'=> User::find(Auth::id())->first()->email,
                'address'=>$input_data['shipping_address'],
                'city'=>$input_data['shipping_city'],
                'state'=>$input_data['shipping_state'],
                'country'=>$input_data['shipping_country'],
                'zipcode'=>$input_data['shipping_zipcode'],
                'mobile'=>$input_data['shipping_mobile']
            ]);
        return redirect()->route('checkout.review');

    }

    public function review_order (){
        $shipping_address=DB::table('delivery_address')->where('user_id',Auth::id())->first();
        $cart_items = Cart::content();
        $i = 0;

        return view('frontend.checkout.review_order',compact('shipping_address', 'cart_items', 'i'));
    }

}
