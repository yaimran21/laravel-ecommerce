@extends('admin.admin_layouts')
@section('title', 'Category Create')
@section('admin_content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title  text-center m-t-0 m-b-30"> Add Category</h4>
                <form method="post" action="{{ route('category.store') }}" enctype="multipart/form-data">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}" placeholder="Enter category name...">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="parent_id">Category Lavel :</label>
                        <div class="controls" >
                            <select name="parent_id" id="parent_id" class="form-control select2">
                                @foreach($cate_levels as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                    <?php
                                    if($key!=0){
                                        $subCategory=DB::table('categories')->select('id','name')->where('parent_id',$key)->get();
                                        if(count($subCategory)>0){
                                            foreach ($subCategory as $subCate){
                                                echo '<option value="'.$subCate->id.'">&nbsp;&nbsp;--'.$subCate->name.'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="description">Description :</label>
                        <div class="controls">
                            <textarea name="description" class="form-control" id="description" rows="3">{{old('description')}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="filer_input2">Category Logo</label>
                        <input type="file" name="logo" class="form-control" id="filer_input">
                    </div>

                    <div class="form-group{{$errors->has('status')?' has-error':''}}">
                        <label class="control-label" for="status">Enable :</label>
                        <input type="checkbox" name="status" id="status" value="1">
                        <span class="text-danger">{{$errors->first('status')}}</span>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                </form>
            </div>
        </div><!-- end col -->
    </div>
@endsection

@section('on_page_js')
    <script>

        $(document).ready(function() {
            $('#filer_input').filer( {
                    limit: 1,
                    maxSize: 3,
                    extensions: ["jpg", "png", "gif"],
                    showThumbs: true
                }
            );
        });
        // Select2
        $(".select2").select2();
    </script>
@endsection
