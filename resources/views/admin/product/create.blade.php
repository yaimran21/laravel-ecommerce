@extends('admin.admin_layouts')
@section('title', 'Product Create')
@section('on_page_css')
@endsection

@section('admin_content')
    <div class="card-box">
        <div class="row">
            <h4 class="display-4  text-center "> Cteate Product</h4>
            <hr/>
            <form method="post" action="{{ route('product.store') }}" enctype="multipart/form-data">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="title">Title <span class="text-danger">*</span></label>
                        <input name="title" type="text"  id="title" class="form-control" value="{{ old('title') }}" placeholder="Enter product title...">
                    </div>

                    <div class="form-group ">
                        <label  for="sku">Sku <span class="text-danger">*</span></label>
                        <input name="sku" type="text" id="sku"  class="form-control" value="{{ old('sku') }}" placeholder="Enter Product sku code...">
                    </div>

                    <div class="form-group ">
                        <label  for="price">Price <span class="text-danger">*</span></label>
                        <input name="price" type="number" min="0" id="price" class="form-control" value="{{ old('price') }}" placeholder="Enter product price...">
                    </div>

                    <div class="form-group ">
                        <label  for="sale_price">Sale Price(If price fall)</label>
                        <input name="sale_price" type="number" min="0" id="sale_price" class="form-control" value="{{ old('sale_price') }}" placeholder="Enter product sale price...">
                    </div>

                    <div class="form-group ">
                        <label  for="quantity">Quantity</label>
                        <input name="quantity" type="number" min="0" id="quantity" class="form-control" value="{{ old('quantity') }}" placeholder="Enter product quantity...">
                    </div>


                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>

                    <div class="form-group clearfix">
                        <label for="filer_input">Image</label>
                        <input type="file" name="image" id="filer_input">
                    </div>

                </div><!-- end col -->

                <div class="col-lg-6">


                    <div class="form-group">
                        <label for="categories_id">Categories</label>
                        <select name="categories_id[]" class=" select2 select2-multiple"  multiple="multiple" multiple id="categories_id" data-plugin="multiselect" data-placeholder="Choose categories ...">
                            @foreach($categories as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>

                                <?php
                                if($key!=0){
                                    $sub_categories=DB::table('categories')->select('id','name')->where('parent_id',$key)->get();
                                    if(count($sub_categories)>0){
                                        foreach ($sub_categories as $sub_category){
                                            echo '<option value="'.$sub_category->id.'">&nbsp;&nbsp;--'.$sub_category->name.'</option>';
                                        }
                                    }
                                }
                                ?>
                            @endforeach
                        </select>
                        <span class="help-block">(you must have to select at least a category)</span>

                    </div>

                    <div class="form-group">
                        <label for="brand_id">Brand</label>
                        <select class="form-control select2" name="brand_id" id="brand_id" data-placeholder="Choose categories ...">
                            <option value="">--Choose brand-- </option>
                            @foreach($brands as $brand)
                                <option value="{{$brand->id}}"> {{ $brand->name }}</option>
                            @endforeach
                        </select>
                        <span class="help-block">(you must have to select at least a brand)</span>

                    </div>

                    <div class="form-group ">
                        <label  for="video_link">Video Link</label>
                        <input name="video_link" type="text" id="video_link" class="form-control" value="{{ old('video_link') }}" placeholder="Enter video link...">
                    </div>

                    <div class="form-group">
                        <label for="details">Description</label>
                        <textarea name="details" id="details" class="form-control summernote" rows="">  {{ old('details') }} </textarea>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </div>

            </form>
        </div>

    </div>
@endsection
@section('on_page_js')


    <script>
        jQuery(document).ready(function(){

            $('.summernote').summernote({
                height: 140,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false                 // set focus to editable area after initializing summernote
            });

            $('.inline-editor').summernote({
                airMode: true
            });
            $('#filer_input').filer( {
                    limit: 1,
                    maxSize: 3,
                    extensions: ["jpg", "png", "gif"],
                    showThumbs: true
                }
            );

            $(".select2").select2();


        });


        <!-- Summernote js-->
    </script>


    {{--    <script>--}}
    {{--        function changeFunc() {--}}
    {{--            var selectBox = document.getElementById("type");--}}
    {{--            var selectedValue = selectBox.options[selectBox.selectedIndex].value;--}}
    {{--            if (selectedValue=="fixed"){--}}
    {{--                $('#fixedValue').show();--}}
    {{--                $('#percentOf').hide();--}}
    {{--            }--}}
    {{--            else if (selectedValue=="percent") {--}}
    {{--                $('#percentOf').show();--}}
    {{--                $('#fixedValue').hide();--}}

    {{--            }--}}
    {{--            else {--}}
    {{--                alert("Select a valid option");--}}
    {{--                $('#percentOf').hide();--}}
    {{--                $('#fixedValue').hide();            }--}}
    {{--        }--}}
    {{--    </script>--}}

    {{--    <script src="{{ asset('backend/pages/jquery.fileuploads.init.js') }}"></script>--}}

@endsection
