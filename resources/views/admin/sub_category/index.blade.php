



@extends('admin.admin_layouts')
@section('title', 'Category index')
@section('admin_content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <h4 style="display: inline-block" class="header-title m-t-0 m-b-30">Sub Category Index</h4>
                <button   type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light " data-toggle="modal" data-target="#category-create-modal"> Create New</button>


                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach( $data['subcategories'] as $subcategory)
                        <tr>
                            <td>{{ $subcategory->id }}</td>
                            <td>{{ $subcategory->name }}</td>
                            <td>{{ $subcategory->category->name }}</td>

                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('subcategory.edit', $subcategory->id) }} ">
                                    <i class="fa fa-edit">
                                    </i>
                                </a>

                                <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="{{ $subcategory->id }}" data-toggle="modal" data-target=".sub_category_delete_modal">
                                    <i id="subCategoryName" data-id="{{ $subcategory->name }}" class="fa fa-trash">
                                    </i>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <div id="category-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form method="post" action="{{ route('subcategory.store') }}">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"> Sub Category Add</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name"> Category</label>
                                    <select class="form-control" name="category_id" required>
                                        <option value="">--Select a category--</option>
                                        @foreach($data['categories'] as $category)
                                            <option value="{{$category->id}}"> {{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div><div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Sub Category Name</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter SubCategory Name" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                    </div>
                </div>
            </form>


        </div>
    </div><!-- /.modal -->


    <!-- Delete Modal -->



    <div class="modal fade sub_category_delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="mySmallModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="sub_category_name">
                    {{--                    <form  action="{{route('category.delete', $category->id)}}" method="post">--}}
                    <form id="deleteForm" action="" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('on_page_js')
    <script>
        $(document).on("click", ".delete-modal", function () {
            let subCategoryId = $(this).data('id');
            let subCategoryName = $('#subCategoryName').data('id');
            $(".modal-body #sub_category_name").text( "Do you want to delete "+ subCategoryName +' Sub Category?' );
            var route = '/admin/subcategory/'+subCategoryId
            $('#deleteForm').attr('action', route)
        });

    </script>
@endsection

