<?php

namespace App\Http\Controllers;

use App\ImageGallery;
use App\Product;
use Illuminate\Http\Request;
use Image;
use Log;

class ImageGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'image' => 'required'

        ]);

        try {
            $inputData=$request->all();
            if($request->file('image')){
                $images=$request->file('image');
                foreach ($images as $image){

                    if($image->isValid()){
                        $extension=$image->getClientOriginalExtension();
                        $filename=rand(100,999999).time().'.'.$extension;
                        $large_image_path=public_path('media/products/large/'.$filename);
                        $medium_image_path=public_path('media/products/medium/'.$filename);
                        $small_image_path=public_path('media/products/small/'.$filename);
                        //// Resize Images
                        Image::make($image)->save($large_image_path);
                        Image::make($image)->resize(600,600)->save($medium_image_path);
                        Image::make($image)->resize(300,300)->save($small_image_path);
//                        dd($request->all());

                        $inputData['image']=$filename;
//                        dd($inputData);
                        $created = ImageGallery::create($inputData);
//                        if ($created){
//                            dd($filename);
//
//                        }

                    }
                }

            }
            $notification=array(
                'message'=>"Add Images Successfully",
                'alert-type'=>'success'
            );
            return back()->with($notification);
        }

        catch (\Exception $e)
        {
            Log::error($e->getMessage());
            $notification=array(
                'message'=>"Image doesn't created",
                'alert-type'=>'error'
            );
            return back()->with($notification);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $i =0;
        $product = Product::findOrFail($id);
        $image_aglleries=ImageGallery::where('product_id',$id)->get();
        return view('admin.product.add_image_gallery',compact('i','product','image_aglleries'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageGallery $imageGallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageGallery $imageGallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImageGallery  $imageGallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $delete=ImageGallery::findOrFail($id);
            $image_large=public_path().'/media/products/large/'.$delete->image;
            $image_medium=public_path().'/media/products/medium/'.$delete->image;
            $image_small=public_path().'/media/products/small/'.$delete->image;
            if($delete->delete()){
                unlink($image_large);
                unlink($image_medium);
                unlink($image_small);
            }
            $notification=array(
                'message'=>"Image deleted",
                'alert-type'=>'success'
            );
            return back()->with($notification);

        }
        catch (\Exception $e){
            Log::error($e->getMessage());
            $notification=array(
                'message'=>"Image doesn't deleted",
                'alert-type'=>'error'
            );
            return back()->with($notification);
        }

    }

}
