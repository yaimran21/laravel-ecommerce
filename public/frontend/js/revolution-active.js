"use strict";

var tpj=jQuery;
var revapi1052;
tpj(document).ready(function() {
    if(tpj("#rev_slider_1052_1").revolution == undefined){
        revslider_showDoubleJqueryError("#rev_slider_1052_1");
    }else{
        revapi1052 = tpj("#rev_slider_1052_1").show().revolution({
            sliderType:"standard",
            jsFileLocation:"js/revolution",
            sliderLayout:"fullscreen",
            dottedOverlay:"none",
            delay:99000,
            navigation: {
                keyboardNavigation:"on",
                keyboard_direction: "vertical",
                mouseScrollNavigation:"off",
                mouseScrollReverse:"default",
                onHoverStop:"off",
                hide_onleave: true,
                hide_delay: 200,
                touch:{
                    touchenabled:"off",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "vertical",
                    drag_block_vertical: false
                },
                arrows: {
                    enable: false,
                    style: 'uranus',
                },
                tabs: {
                    enable: true,
                    style: 'zeus',
                    tmp: '<span class="tp-tab-title">{{title}}</span>',
                    width: 100,
                    height: 40,
                    min_width: 100,
                    visibleAmount: 5,
                    rtl: true,
                    direction: 'vertical',
                    position: 'inner',
                    space: 0,
                    h_align: 'right',
                    v_align: 'center',
                    h_offset: 0,
                    v_offset: 20,
                    span: true,
                    wrapper_padding: 0,
                    wrapper_color: 'transparent',
                    wrapper_opacity: '0',
                    hide_onleave: false,
                    hide_onmobile: true,
                    hide_under: 992,
                    hide_over: 9999,
                    hide_delay: 200,
                    hide_delay_mobile: 1200

                }
            },
            responsiveLevels:[1240,1025,778,480],
            visibilityLevels:[1240,1025,778,480],
            gridwidth:[1400,1240,778,480],
            gridheight:[500,400,300,300],
            lazyType:"none",
            shadow:0,
            spinner:"on",
            stopLoop:"on",
            stopAfterLoops:-1,
            stopAtSlide:-1,
            shuffle:"off",
            autoHeight:"off",
            fullScreenAutoWidth:"off",
            fullScreenAlignForce:"off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "",
            disableProgressBar:"on",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
                simplifyAll:"off",
                nextSlideOnWindowFocus:"off",
                disableFocusListener:false,
            }
        });
    }
});	/*ready*/
