@extends('admin.admin_layouts')
@section('title', 'Category index')
@section('admin_content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                <h4 style="display: inline-block" class="header-title">Product: {{ $product->title }}</h4>
                <table class="table m-0">
                    <tbody>
                    <tr>
                        <td>
                            <img src="{{url('media/products/small',$product->image)}}" alt="" width="40" height="40">
                            <div style="display: inline-block">
                                <span>Product Code : <b>{{$product->sku}}</b></span>
                                <p>Product Status : <b>{{$product->status? 'Active': 'Inactive' }}</b></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                                <form action="{{route('imagegallery.store')}}" method="post" role="form" enctype="multipart/form-data">
                                    <legend>Can Add Multi Images</legend>
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                        <input type="file" name="image[]" id="id_imageGallery" class="form-control" multiple="multiple" required>
                                        <span class="text-danger d-flex">{{$errors->first('image')}}</span>
                                        <button type="submit" class="btn btn-success">Upload</button>
                                    </div>
                                </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="card-box table-responsive text-center">
                <h4 style="display: inline-block" class="display-4">Images List</h4>
                {{--                <hr>--}}
                {{--                <button   type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light " data-toggle="modal" data-target="#category-create-modal"> Create New</button>--}}
                <table id="alignMiddleTable" class="table table-bordered ">
                    <thead>
                    <tr >
                        <th>Id#</th>
                        <th class="text-center">Image</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>

                    <tbody class="text-center">
                    @foreach($image_aglleries as $img_gallery)
                        <?php $i++; ?>
                        <tr>
                            <th scope="row">{{$i}}</th>
                            <td><img src="{{url('media/products/small',$img_gallery->image)}}" class="img-responsive" alt="Image" width="60"></td>
                            <td>
                                <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="{{ $img_gallery->id }}" data-toggle="modal" data-target=".img_delete_modal">
                                    <i id="product_title" data-id="{{ $img_gallery->image }}" class="fa fa-trash">
                                    </i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->



    </div>
    <!-- end row -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    {{--    <div id="category-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">--}}
    {{--        <div class="modal-dialog">--}}
    {{--            <form method="post" action="{{ route('category.store') }}">--}}
    {{--                @csrf--}}
    {{--                <div class="modal-content">--}}
    {{--                    <div class="modal-header">--}}
    {{--                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
    {{--                        <h4 class="modal-title"> Create Category</h4>--}}
    {{--                    </div>--}}
    {{--                    <div class="modal-body">--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-md-12">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label for="name" class="control-label">Name</label>--}}
    {{--                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter category name.." required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="modal-footer">--}}
    {{--                        <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </form>--}}


    {{--        </div>--}}
    {{--    </div><!-- /.modal -->--}}


    <!-- Delete Modal -->
    <div class="modal fade img_delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="mySmallModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="category_name">
                    <form id="deleteForm" action="" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('on_page_js')
    <script>
        $(document).on("click", ".delete-modal", function () {
            let imageID = $(this).data('id');
            let product_title = $('#product_title').data('id');
            console.log(imageID);
            console.log(product_title);
            $(".modal-body #category_name").text( "Do you want to delete "+ product_title +' image?' );
            var route = '/admin/imagegallery/'+imageID
            $('#deleteForm').attr('action', route)
        });

    </script>
    <script>

        $(document).ready(function() {
            $('#id_imageGallery').filer( {
                    limit:10,
                    maxSize: 3,
                    extensions: ["jpg", "png", "gif", 'jpeg'],
                    showThumbs: true
                }
            );
        });
        // Select2
        $(".select2").select2();
    </script>
@endsection
