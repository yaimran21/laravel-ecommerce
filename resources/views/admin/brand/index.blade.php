@extends('admin.admin_layouts')
@section('title', 'Category index')
@section('admin_content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <h4 style="display: inline-block" class="header-title m-t-0 m-b-30">Brand Index</h4>
                <button   type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light " data-toggle="modal" data-target="#category-create-modal"> Create New</button>


                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Logo</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($brands as $brand)
                        <tr>
                            <td>{{ $brand->id }}</td>
                            <td>{{ $brand->name }}</td>
                            <td>
                                <img class="" src="{{ URL::to($brand->logo) }}" style="width:45px; height: 25px;">

                            </td>


                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('brand.edit', $brand->id) }} ">
                                    <i class="fa fa-edit">
                                    </i>
                                </a>

                                <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="{{ $brand->id }}" data-toggle="modal" data-target=".brand_delete_modal">
                                    <i id="brandName" data-id="{{ $brand->name }}" class="fa fa-trash">
                                    </i>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <div id="category-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form method="post" action="{{ route('brand.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"> Create Brand</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter brand name.." required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="logo">Brand Logo</label>
                                    <input type="file" name="logo" class="form-control" id="logo" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                    </div>
                </div>
            </form>


        </div>
    </div><!-- /.modal -->


    <!-- Delete Modal -->



    <div class="modal fade brand_delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="mySmallModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="category_name">
                    <form id="deleteForm" action="" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('on_page_js')
    <script>
        $(document).on("click", ".delete-modal", function () {
            let brandId = $(this).data('id');
            let brandName = $('#brandName').data('id');
            $(".modal-body #category_name").text( "Do you want to delete "+ brandName +' brand?' );
            var route = '/admin/brand/'+brandId
            $('#deleteForm').attr('action', route)
        });

    </script>
@endsection
