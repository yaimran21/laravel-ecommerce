@extends('admin.admin_layouts')
@section('title', 'Category index')
@section('admin_content')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                <h4 style="display: inline-block" class="header-title">Product: {{ $product->title }}</h4>
                <table class="table m-0">
                    <tbody>
                    <tr>
                        <td>
                            <img class="text-center" src="{{url('media/products/small',$product->image)}}" alt="" width="40" height="40">
                            <div style="display: inline-block">
                                <span>Product Code : <b>{{$product->sku}}</b></span>
                                <p>Product Status : <b>{{$product->status? 'Active': 'Inactive' }}</b></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form action="{{route('attribute.store')}}" method="post" role="form">
                                <legend>Add Attribute</legend>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <input type="text" class="form-control" name="sku" value="{{old('sku')}}" id="sku" placeholder="SKU" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="size" value="{{old('size')}}" id="size" placeholder="Size" required>

                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="price" value="{{old('price')}}" id="price" placeholder="Price" required>
                                            <span style="color: red;">{{$errors->first('price')}}</span>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" class="form-control" name="stock" value="{{old('stock')}}" id="stock" placeholder="Stock" required>

                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success">Add</button>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="card-box table-responsive text-center">
                <h4 style="display: inline-block" class="display-4">Product Attr List</h4>
                <form action="{{route('attribute.update',$product->id)}}" method="post" role="form">
                    @method('PUT')
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <table id="alignMiddleTable" class="table table-bordered ">
                        <thead>
                        <tr >
                            <th>Sku</th>
                            <th>Size</th>
                            <th>Price</th>
                            <th>Stock</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>

                        <tbody class="text-center">
                        @foreach($attributes as $attribute)
                            <input type="hidden" name="id[]" value="{{$attribute->id}}">
                            <tr>
                                <td>
                                    <input type="text" name="sku[]" id="sku" class="form-control" value="{{$attribute->sku}}" required="required" style="width: 100px;">
                                </td>
                                <td>
                                    <input type="text" name="size[]" id="size" class="form-control" value="{{$attribute->size}}" required="required" style="width: 75px;">

                                </td>
                                <td>
                                    <input type="text" name="price[]" id="price" class="form-control" value="{{$attribute->price}}" required="required" style="width: 75px;">
                                </td>
                                <td>
                                    <input type="text" name="stock[]" id="stock" class="form-control" value="{{$attribute->stock}}" required="required" style="width: 75px;">
                                </td>
                                <td>
                                    <a>
                                        <button type="submit" class="btn btn-xs btn-success">Update</button>
                                    </a>
                                    <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="{{ $attribute->id }}" data-toggle="modal" data-target=".attr_delete_modal">
                                        <i id="attr_title" data-id="{{ $attribute->sku }}" class="fa fa-trash">
                                        </i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </form>
            </div>
        </div><!-- end col -->



    </div>
    <!-- end row -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    {{--    <div id="category-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">--}}
    {{--        <div class="modal-dialog">--}}
    {{--            <form method="post" action="{{ route('category.store') }}">--}}
    {{--                @csrf--}}
    {{--                <div class="modal-content">--}}
    {{--                    <div class="modal-header">--}}
    {{--                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
    {{--                        <h4 class="modal-title"> Create Category</h4>--}}
    {{--                    </div>--}}
    {{--                    <div class="modal-body">--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-md-12">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label for="name" class="control-label">Name</label>--}}
    {{--                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter category name.." required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="modal-footer">--}}
    {{--                        <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </form>--}}


    {{--        </div>--}}
    {{--    </div><!-- /.modal -->--}}


    <!-- Delete Modal -->
    <div class="modal fade attr_delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="mySmallModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="attr_name">
                    <form id="deleteForm" action="" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('on_page_js')
    <script>
        $(document).on("click", ".delete-modal", function () {
            let attr_id = $(this).data('id');
            let attr_title = $('#attr_title').data('id');
            console.log(attr_id);
            console.log(attr_title);
            $(".modal-body #attr_name").text( "Do you want to delete "+ attr_title +' attribute?' );
            var route = '/admin/attribute/'+attr_id
            $('#deleteForm').attr('action', route)
        });

    </script>
@endsection
