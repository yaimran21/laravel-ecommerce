<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                <li class="text-muted menu-title">Navigation</li>

                <li class="has_sub">
                    <a href="{{ route('admin.home') }}" class="waves-effect"><i class="fa fa-home"></i>
                        <span> Dashboard </span> </a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="fa fa-indent"></i>

                        <span> Category </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('category.index') }}">List</a></li>
                        <li><a href="{{ route('category.create') }}">Create</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-check-circle-o" aria-hidden="true"></i>
                        <span> Brand </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('brand.index') }}">List</a></li>
                        <li><a href="{{ route('brand.create') }}">Create</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-product-hunt" aria-hidden="true"></i>

                        <span> Product </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('product.index') }}">List</a></li>
                        <li><a href="{{ route('product.create') }}">Create</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tags"></i>
                        <span> Coupon </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('coupon.index') }}">List</a></li>
                        <li><a href="{{ route('coupon.create') }}">Create</a></li>
                    </ul>
                </li>




{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> SubCategory </span> <span class="menu-arrow"></span> </a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="ui-buttons.html">Buttons</a></li>--}}
{{--                        <li><a href="ui-cards.html">Cards</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Brand </span> <span class="menu-arrow"></span> </a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="ui-buttons.html">Buttons</a></li>--}}
{{--                        <li><a href="ui-cards.html">Cards</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}


{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> User Interface </span> <span class="menu-arrow"></span> </a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="ui-buttons.html">Buttons</a></li>--}}
{{--                        <li><a href="ui-cards.html">Cards</a></li>--}}
{{--                        <li><a href="ui-typography.html">Typography </a></li>--}}
{{--                        <li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>--}}
{{--                        <li><a href="ui-icons.html">Icons</a></li>--}}
{{--                        <li><a href="ui-modals.html">Modals</a></li>--}}
{{--                        <li><a href="ui-images.html">Images</a></li>--}}
{{--                        <li><a href="ui-components.html">Components</a>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-case"></i> <span> Admin UI </span> <span class="menu-arrow"></span> </a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="admin-masonry.html">Masonry</a></li>--}}
{{--                        <li><a href="admin-notification.html">Notification</a></li>--}}
{{--                        <li><a href="admin-range-slider.html">Range Slider</a></li>--}}
{{--                        <li><a href="admin-sweetalert.html">Sweet Alert</a>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="widgets.html" class="waves-effect"><i class="zmdi zmdi-layers"></i> <span> Widgets </span> </a>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-text"></i><span class="label label-default pull-right">6</span><span> Forms </span> </a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="form-elements.html">Form Elements</a></li>--}}
{{--                        <li><a href="form-advanced.html">Advanced Form</a></li>--}}
{{--                        <li><a href="form-validation.html">Form Validation</a></li>--}}
{{--                        <li><a href="form-wizard.html">Form Wizard</a></li>--}}
{{--                        <li><a href="form-summernote.html">Summernote</a></li>--}}
{{--                        <li><a href="form-uploads.html">Form Uploads</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-list"></i> <span> Tables </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="tables-basic.html">Basic Tables</a></li>--}}
{{--                        <li><a href="tables-datatable.html">Data Table</a></li>--}}
{{--                        <li><a href="tables-editable.html">Editable Table</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-chart"></i><span> Charts </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="chart-flot.html">Flot Chart</a></li>--}}
{{--                        <li><a href="chart-morris.html">Morris Chart</a></li>--}}
{{--                        <li><a href="chart-chartist.html">Chartist Charts</a></li>--}}
{{--                        <li><a href="chart-other.html">Other Chart</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-map"></i><span> Maps </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="map-google.html">Google Maps</a></li>--}}
{{--                        <li><a href="map-vector.html">Vector Maps</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="text-muted menu-title">More</li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-item"></i><span class="label label-default pull-right">8</span><span> Pages </span></a>--}}
{{--                    <ul class="list-unstyled">--}}
{{--                        <li><a href="page-starter.html">Starter Page</a></li>--}}
{{--                        <li><a href="page-timeline.html">Timeline</a></li>--}}
{{--                        <li><a href="page-login.html">Login</a></li>--}}
{{--                        <li><a href="page-register.html">Register</a></li>--}}
{{--                        <li><a href="page-recoverpw.html">Recover Password</a></li>--}}
{{--                        <li><a href="page-lock-screen.html">Lock Screen</a></li>--}}
{{--                        <li><a href="page-confirm-mail.html">Confirm Mail</a></li>--}}
{{--                        <li><a href="page-404.html">Error 404</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="has_sub">--}}
{{--                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-share"></i><span> Multi Level </span> <span class="menu-arrow"></span></a>--}}
{{--                    <ul>--}}
{{--                        <li class="has_sub">--}}
{{--                            <a href="javascript:void(0);" class="waves-effect"><span>Menu Level 1.1</span> <span class="menu-arrow"></span></a>--}}
{{--                            <ul style="">--}}
{{--                                <li><a href="javascript:void(0);"><span>Menu Level 2.1</span></a></li>--}}
{{--                                <li><a href="javascript:void(0);"><span>Menu Level 2.2</span></a></li>--}}
{{--                                <li><a href="javascript:void(0);"><span>Menu Level 2.3</span></a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="javascript:void(0);"><span>Menu Level 1.2</span></a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
<!-- Left Sidebar End -->
