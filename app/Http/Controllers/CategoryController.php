<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Image;
use Log;

class CategoryController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }


    public function index(){
        $id = 0;
        $categories = Category::all();
        return view('admin.category.index', compact('categories', 'id'));
    }

    public  function create(){
        $plucked=Category::where('parent_id',0)->pluck('name','id');
        $cate_levels=['0'=>'Main Category']+$plucked->all();
        return view('admin.category.create',compact('cate_levels'));    }

    public function store (Request $request){
        $validatedData = $request->validate([
            'name' => 'required|unique:categories|max:255',
//            'logo' => 'required'

        ]);


        try{
            $formInput=$request->all();
            if($request->file('logo')){
                $image=$request->file('logo');
                if($image->isValid()){
                    $fileName=time().'-'.str_slug($formInput['name'],"-").'.'.$image->getClientOriginalExtension();
                    $large_image_path=public_path('media/category/large/'.$fileName);
                    $medium_image_path=public_path('media/category/medium/'.$fileName);
                    $small_image_path=public_path('media/category/small/'.$fileName);
                    //Resize Image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600,600)->save($medium_image_path);
                    Image::make($image)->resize(300,300)->save($small_image_path);
                    $formInput['logo']=$fileName;
                }
            }
            $formInput['status'] = $request['status']? 1: 0;
            $category = Category::create($formInput);
            $category->save();
            $notification=array(
                'message'=>"Category created sucessfully",
                'alert-type'=>'success'
            );
            return Redirect()->route('category.index')->with($notification);
        }
        catch(Exception $e){
            $notification=array(
                'message'=>"Category doesn't created",
                'alert-type'=>'error'
            );

            return Redirect()->back()->with($notification);

        }




    }


    public function edit($id){
        $menu_active=0;
        $plucked=Category::where('parent_id',0)->pluck('name','id');
        $cate_levels=['0'=>'Main Category']+$plucked->all();
        $category=Category::findOrFail($id);
        return view('admin.category.edit',compact('category','menu_active','cate_levels'));
    }


    public function update(Request $request, $id) {

        try {
            $update_category=Category::findOrFail($id);

            $formInput=$request->all();
//            dd($formInput);
            if($request->file('logo')){
                $image=$request->file('logo');
                if($image->isValid()){
                    if ($update_category->logo) {
//                        dd($update_category->logo);
                        //Unlink previous photos
                        $image_large=public_path().'/media/category/large/'.$update_category->logo;
                        $image_medium=public_path().'/media/category/medium/'.$update_category->logo;
                        $image_small=public_path().'/media/category/small/'.$update_category->logo;
//                    dd($image_small);
                        unlink($image_large);
                        unlink($image_medium);
                        unlink($image_small);
                    }

//                    dd($image_large);

                    $fileName=time().'-'.str_slug($formInput['name'],"-").'.'.$image->getClientOriginalExtension();
                    $large_image_path=public_path('media/category/large/'.$fileName);
                    $medium_image_path=public_path('media/category/medium/'.$fileName);
                    $small_image_path=public_path('media/category/small/'.$fileName);
                    //Resize Image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(645,807)->save($medium_image_path);
                    Image::make($image)->resize(335,405)->save($small_image_path);
                    $formInput['logo']=$fileName;

                }
            }
            $formInput['status'] = $request['status']? 1: 0;;
            $update_category->update($formInput);

//                $category->name = $input_data['name'];
//                $category->description = $input_data['description'];
//                $category->parent_id = $input_data['parent_id'];
//                $input_data['status'] = isset($input_data['status'])? $input_data['status']: 0;

            return redirect()->route('category.index')->with('message','Updated Success!');

        }


        catch (\Exception $e){
            Log::error($e->getMessage());
            return redirect()->route('category.index')->with('message', "Category doesn't updated ");

        }

    }



    public function delete($id){
        $category = Category::findOrFail($id);

        $category->delete();
        $notification=array(
            'message'=>"Category deleted",
            'alert-type'=>'error'
        );

        return Redirect()->back()->with($notification);
    }


}
