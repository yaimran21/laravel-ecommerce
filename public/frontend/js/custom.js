 jQuery(document).ready(function () {
    'use strict';
    /*** =====================================
    * Mixitup
    * =====================================***/
    $('#mixitup-grid').mixItUp();
    $('.filter-options li:first-child a').addClass("active");

    /*** =====================================
    * 	Mobile Menu
    * =====================================***/
	$('.mobile-background-nav .has-submenu').on('click',function(e) {
	  	e.preventDefault();
	    var $this = $(this);
	    if ($this.next().hasClass('menu-show')) {
	        $this.next().removeClass('menu-show');
	        $this.next().slideUp(350);
	    } else {
	        $this.parent().parent().find('li .dropdown').removeClass('menu-show');
	        $this.parent().parent().find('li .dropdown').slideUp(350);
	        $this.next().toggleClass('menu-show');
	        $this.next().slideToggle(350);
	    }
	});
	$('.mobile-menu-close i').on('click',function(){
	     $('.mobile-background-nav').removeClass('in');
	});

	$('#humbarger-icon').on('click',function(e){
        e.preventDefault();
	     $('.mobile-background-nav').toggleClass('in');
	});

     /*** =====================================
      * 	Mobile Menu
      * =====================================***/
     $('.mobile-category-nav .has-submenu').on('click',function(e) {
         e.preventDefault();
         var $this = $(this);
         if ($this.next().hasClass('menu-show')) {
             $this.next().removeClass('menu-show');
             $this.next().slideUp(350);
         } else {
             $this.parent().parent().find('li .dropdown').removeClass('menu-show');
             $this.parent().parent().find('li .dropdown').slideUp(350);
             $this.next().toggleClass('menu-show');
             $this.next().slideToggle(350);
         }
     });
     $('.mobile-menu-close i').on('click',function(){
         $('.mobile-category-nav').removeClass('in');
     });

     $('#category-icon').on('click',function(e){
         e.preventDefault();
         $('.mobile-category-nav').toggleClass('in');
     });

    /*** =====================================
    * Preloder
    * ==================================== ***/
	$(window).on('load', function(){
        /** ===== Preloder ========**/
	    $('.preloader').fadeOut();
	});
    /*** =====================================
    * Easy Menu
    * =====================================***/
	(function($) {
	    $.fn.menumaker = function(options) {
	        var cssmenu = $(this),
	            settings = $.extend({
	                format: "dropdown",
	                sticky: false
	            }, options);
	        return this.each(function() {
	            $(this).find(".button").on('click', function() {
	                $(this).toggleClass('menu-opened');
	                var mainmenu = $(this).next('ul');
	                if (mainmenu.hasClass('open')) {
	                    mainmenu.slideToggle().removeClass('open');
	                } else {
	                    mainmenu.slideToggle().addClass('open');
	                    if (settings.format === "dropdown") {
	                        mainmenu.find('ul').show();
	                    }
	                }
	            });
	            cssmenu.find('li ul').parent().addClass('has-sub');
	            var multiTg;
	            multiTg = function() {
	                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
	                cssmenu.find('.submenu-button').on('click', function() {
	                    $(this).toggleClass('submenu-opened');
	                    if ($(this).siblings('ul').hasClass('open')) {
	                        $(this).siblings('ul').removeClass('open').slideToggle();
	                    } else {
	                        $(this).siblings('ul').addClass('open').slideToggle();
	                    }
	                });
	            };
	            if (settings.format === 'multitoggle') multiTg();
	            else cssmenu.addClass('dropdown');
	            if (settings.sticky === true) cssmenu.css('position', 'fixed');
	            var resizeFix;
	            resizeFix = function() {
	                var mediasize = 1000;
	                if ($(window).width() > mediasize) {
	                    cssmenu.find('ul').show();
	                }
	                if ($(window).width() <= mediasize) {
	                    cssmenu.find('ul').hide().removeClass('open');
	                }
	            };
	            resizeFix();
	            return $(window).on('resize', resizeFix);
	        });
	    };
	})(jQuery);
	 $("#easy-menu").menumaker({
        format: "multitoggle"
    });
    $('[data-toggle="tooltip"]').tooltip();
    /** =====================================
    *  Popover
    * ===================================== **/
    var ops = {
        'html':true,
        content: function(){
            return $('#popover-one').html();
        }
    };
    var ops2 = {
        'html':true,
        content: function(){
            return $('#popover-two').html();
        }
    };
    var ops3 = {
        'html':true,
        content: function(){
            return $('#popover-three').html();
        }
    };
    var ops4 = {
        'html':true,
        content: function(){
            return $('#popover-four').html();
        }
    };
    $(function(){
        $('#plus-one').popover(ops);
        $('#plus-two').popover(ops2)
        $('#plus-three').popover(ops3)
        $('#plus-four').popover(ops4)
    });


    /** =====================================
    *  Back to top
    * ===================================== **/
    $(window).scroll(function(){
        if ($(this).scrollTop()>10) {
            $('#toTop').addClass('backtop-top-show');
        } else {
            $('#toTop').removeClass('backtop-top-show');
        }
    })
    $("#toTop").click(function (e) {
        e.preventDefault();
       $("html, body").animate({scrollTop: 0}, 1000);
    });
    /**=====================================
   *  carousel
   * =====================================*/
   var owlSlider = $('.owl');
   owlSlider.each( function() {
       var $carousel = $(this);
       $carousel.owlCarousel({
           nav :JSON.parse($carousel.attr('data-navigation')),
           dots: JSON.parse($carousel.attr('data-pagination')),
           autoplay: JSON.parse($carousel.attr('data-autoplay')),
           loop:true,
           margin:JSON.parse($carousel.attr('data-margin')),
           navText: [
               "<i class='fa fa-angle-left'></i>",
               "<i class='fa fa-angle-right'></i>"
           ],
           responsive:{
              0:{
                  items:$carousel.attr('data-itemsMobile'),
              },
              480:{
                  items:$carousel.attr('data-itemsTablet'),
              },
              768:{
                  items:$carousel.attr('data-itemsDesktopSmall'),
              },
              1200:{
                  items:$carousel.attr('data-itemsDesktop'),
              }
          }
       });
   });
   /** =====================================
    *   Search Box
    * =====================================**/
   	$('.search-box a').on('click', function(e) {
        e.preventDefault();
        $('.top-search-input-wrap').addClass('show');
   	});
   	$(".top-search-input-wrap .top-search-overlay, .top-search-input-wrap .close-icon").on('click', function(){
        $('.top-search-input-wrap').removeClass('show');
   	});
    /** =====================================
	*  Shop Item Cart
	* ===================================== **/
	$('.item-quantity .increment-button').on('click', function(){
		var pqty = $(this).parents('.item-quantity').find('.product-quantity').val();
		pqty++;
		$(this).parents('.item-quantity').find('.product-quantity').val(pqty);
	});
	$('.item-quantity .decrement-button').on('click', function(){
		var pqty = $(this).parents('.item-quantity').find('.product-quantity').val();
		if(pqty>1){
			pqty--;
		}
		$(this).parents('.item-quantity').find('.product-quantity').val(pqty);
	});
    // Inline popups
    $('.magnificPopup-wrapper').magnificPopup({
      delegate: '.magnific-popup-active',
      removalDelay: 500, //delay removal by X to allow out-animation
      callbacks: {
        beforeOpen: function() {
           this.st.mainClass = this.st.el.attr('data-effect');
        }
      },
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
    /** =====================================
    *  Wow JS
    * ===================================== **/
    if($('.wow').length){
        var wow=new WOW( {
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0, // distance to the element when triggering the animation (default is 0)
            mobile: false, // trigger animations on mobile devices (default is true)
            live: true, // act on asynchronously loaded content (default is true)
            callback: function(box) {}
            , scrollContainer: true // optional scroll container selector, otherwise use window
        }
        );
       wow.init();
    }
    /*** =====================================
    *  Event Counter
    * ===================================== ***/
    var counterItem = $('.counter-active');
    counterItem.each( function() {
        var $counter = $(this);
        var dataDate =JSON.parse($counter.attr('data-date'));
        var dataMonth= JSON.parse($counter.attr('data-month'));
        var dataYear= JSON.parse($counter.attr('data-year'));
        dataMonth=dataMonth-1;
         $counter.countdown({
             until: $.countdown.UTCDate(+10, dataYear, dataMonth, dataDate),
             format: 'dHms',
             padZeroes: false,
             labels: ['Years', 'Months', 'Weeks', 'Days', 'Hour', 'Mins', 'Secs'],
         });

    });


    // if($('.counter-active').length){
    //     $('#event-one-counter').countdown({
    //         until: $.countdown.UTCDate(+10, 2017, 5 - 1, 28),
    //         format: 'dHM',
    //         padZeroes: true
    //     });
    // }
    /** =====================================
    *  Wow JS
    * ===================================== **/
    if($('#slider-range').length){
        $( "#slider-range" ).slider({
          range: true,
          min: 0,
          max: 500,
          values: [ 0, 300 ],
          slide: function( event, ui ) {
            $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
    		$( "#amount1" ).val(ui.values[ 0 ]);
    		$( "#amount2" ).val(ui.values[ 1 ]);
          }
        });
        $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
         " - $" + $( "#slider-range" ).slider( "values", 1 ) );
    }
    /** =====================================
    *  Loading Wave
    * ===================================== **/
    if($('.loading-wave-animation').length){
        /**
         * Equation of a line.
         */
        const lineEqualition = (y2, y1, x2, x1, currentVal) => {
            var m = (y2 - y1) / (x2 - x1), b = y1 - m * x1;
            return m * currentVal + b;
        };
        const distanceCalculate = {min: 0, max: 100};
            /**************** Link2 ("wilderness") ****************/
            const link2 = document.getElementById('link2');
            const tooltipWave = link2.querySelector('.loading-wave-animation > span');
            const waveInterval = {from: 1, to: 15};
            const tweenWave = TweenMax.to(tooltipWave, 15, {
                ease: 'Linear.easeNone',
                repeat: -1,
                yoyo: false,
                x: '50%',
                paused: true
            });
            let stateWave= 'paused';
            new Nearby(link2, {
                onProgress: (distance) => {
                    const time = lineEqualition(waveInterval.from, waveInterval.to, distanceCalculate.max, distanceCalculate.min, distance);
                    tweenWave.timeScale(Math.min(Math.max(time,waveInterval.from),waveInterval.to));

                    if ( distance < distanceCalculate.max && distance >= distanceCalculate.min && stateWave !== 'running' ) {
                        tweenWave.play();
                        stateWave = 'running';
                    }
                    else if ( (distance > distanceCalculate.max || distance < distanceCalculate.min) && stateWave !== 'paused' ) {
                        tweenWave.pause();
                        stateWave = 'paused';
                    }
                }
            });
    }



});
