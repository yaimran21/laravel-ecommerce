<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Brand;
use Image;
use Log;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i=0;
        $products=Product::orderBy('created_at','desc')->get();
        return view('admin.product.index',compact('products','i'));    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands =Brand::all();
        $categories=Category::where('parent_id',0)->pluck('name','id')->all();
        return view('admin.product.create',  compact('brands', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'categories_id' => 'required|max:255',
            'title' => 'required|max:255',
            'brand_id' => 'required|max:255',
            'sku' => 'required|max:255',
            'quantity' => 'required|max:255',
            'price' => 'required|numeric',
            'image'=>'image|mimes:png,jpg,jpeg|max:1000',
        ]);

        try {
            $formInput=$request->all();
            if($request->file('image')){
                $image=$request->file('image');
                if($image->isValid()){
                    $fileName=time().'-'.str_slug($formInput['title'],"-").'.'.$image->getClientOriginalExtension();
                    $large_image_path=public_path('media/products/large/'.$fileName);
                    $medium_image_path=public_path('media/products/medium/'.$fileName);
                    $small_image_path=public_path('media/products/small/'.$fileName);
                    //Resize Image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(645,807)->save($medium_image_path);
                    Image::make($image)->resize(335,405)->save($small_image_path);
                    $formInput['image']=$fileName;
                }
            }
            $product = Product::create($formInput);
            if ($product){
                $product->categories()->sync($formInput['categories_id']);
            }

            return redirect()->route('product.create')->with('message','Add Products Successfully!');

        }
        catch (\Exception $e){
            Log::error($e->getMessage());
            return redirect()->route('product.create')->with('error','Something went wrong..');

        }



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = Brand::all();
        $categories=Category::where('parent_id',0)->pluck('name','id')->all();
        $product=Product::findOrFail($id);
        $edit_categories = $product->categories;

        return view('admin.product.edit', compact('categories', 'product', 'edit_categories', 'brands'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'categories_id' => 'required|max:255',
            'title' => 'required|max:255',
            'brand_id' => 'required|max:255',
            'sku' => 'required|max:255',
            'quantity' => 'required|max:255',
            'price' => 'required|numeric',
            'image'=>'image|mimes:png,jpg,jpeg|max:2024',
        ]);

        try {
            $update_product=Product::findOrFail($id);
            $formInput=$request->all();
            if($update_product['image']==''){
                if($request->file('image')){
                    $image=$request->file('image');
                    if($image->isValid()){
                        $fileName=time().'-'.str_slug($formInput['title'],"-").'.'.$image->getClientOriginalExtension();
                        $large_image_path=public_path('media/products/large/'.$fileName);
                        $medium_image_path=public_path('media/products/medium/'.$fileName);
                        $small_image_path=public_path('media/products/small/'.$fileName);
                        //Resize Image
                        Image::make($image)->save($large_image_path);
                        Image::make($image)->resize(600,600)->save($medium_image_path);
                        Image::make($image)->resize(300,300)->save($small_image_path);
                        $formInput['image']=$fileName;
                    }
                }
            }else{
                $formInput['image']=$update_product['image'];
            }

            $update_product->update($formInput);
            if ($update_product){
                $update_product->categories()->sync($formInput['categories_id']);
            }
            $notification=array(
                'message'=>"Product updated",
                'alert-type'=>'success'
            );
            return redirect()->route('product.index')->with($notification);
        }
    catch (\Exception $e){
            Log::error($e->getMessage());
//        dd($formInput=$request->all());

        $notification=array(
            'message'=>"Product doesn't updated",
            'alert-type'=>'error'
        );
        dd($notification);
        return redirect()->route('product.edit', $update_product->id)->with($notification);

    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        try {
            $delete_product=Product::findOrFail($id);
            $image_large=public_path().'/media/products/large/'.$delete_product->image;
            $image_medium=public_path().'/media/products/medium/'.$delete_product->image;
            $image_small=public_path().'/media/products/small/'.$delete_product->image;

            $delete_product->categories()->detach();
            $delete_product->delete();
            $notification=array(
                'message'=>"Product deleted",
                'alert-type'=>'error'
            );

            return Redirect()->back()->with($notification);
        }

        catch (\Exception $e) {
            Log::error($e->getMessage());
            $notification=array(
                'message'=>"Product doesn't deleted",
                'alert-type'=>'error'
            );

            return Redirect()->back()->with($notification);
        }

    }



    public function deleteImage($id){
        //Products_model::where(['id'=>$id])->update(['image'=>'']);
        $product=Product::findOrFail($id);
        $image_large=public_path().'/media/products/large/'.$product->image;
        $image_medium=public_path().'/media/products/medium/'.$product->image;
        $image_small=public_path().'/media/products/small/'.$product->image;
        if($product){
            $product->image='';
            $product->save();
            ////// delete image ///
            unlink($image_large);
            unlink($image_medium);
            unlink($image_small);
        }
        return back();
    }
}
