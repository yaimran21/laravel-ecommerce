@extends('admin.admin_layouts')
@section('title', 'Category Create')
@section('admin_content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title  text-center m-t-0 m-b-30"> Add Sub Category</h4>
                <form method="post" action="{{ route('subcategory.store') }}">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="category_id"> Category</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    <option value="">--Select a category--</option>
                                @foreach($categories as $category)
                                        <option value="{{$category->id}}"> {{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Sub Category Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter SubCategory Name">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                </form>
            </div>
        </div><!-- end col -->
    </div>
@endsection



{{--<div id="category-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">--}}
{{--    <div class="modal-dialog">--}}
{{--        <form method="post" action="{{ route('subcategory.store') }}">--}}
{{--            @csrf--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
{{--                    <h4 class="modal-title"> Sub Category Add</h4>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="name"> Category</label>--}}
{{--                                <select class="form-control" name="category_id">--}}
{{--                                    @foreach($data['categories'] as $category)--}}
{{--                                        <option value="{{$category->id}}"> {{ $category->name }}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="name">Sub Category Name</label>--}}
{{--                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter SubCategory Name">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="modal-footer">--}}
{{--                    <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}


{{--    </div>--}}
{{--</div><!-- /.modal -->--}}
