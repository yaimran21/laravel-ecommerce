<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = [
        'user_id','address','city','state','country','zipcode','mobile'
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }
}
