<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class SubCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $data =  array();
        $data['subcategories']  =  SubCategory::all();
        $data['categories']     =  Category::all();

        return view('admin.sub_category.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::all();

        return view('admin.sub_category.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request){
        $validatedData = $request->validate([
            'name' => 'required|unique:sub_categories|max:255',
            'category_id' => 'required|max:255',
        ]);

        try{
            $sub_category = new SubCategory();
            $sub_category->name = $request['name'];
            $sub_category->category_id = $request['category_id'];
            $sub_category->save();

            $notification=array(
                'message'=>'Sub Category created successfully',
                'alert-type'=>'success'
            );

            return Redirect()->route('subcategory.index')->with($notification);
        }
        catch(Exception $e){
            $notification=array(
                'message'=>"Sub Category doesn't created",
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = SubCategory::where('id', $id)->first();
        return view('admin.sub_category.edit')->withSubcategory($subcategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        try{
            $validatedData = $request->validate([
                'name' => 'required|unique:categories|max:255',
            ]);

            $sub_category = SubCategory::where('id', $id)->first();
            $sub_category->name = $request['name'];
            $update = $sub_category->save();

            $notification=array(
                'message'=>"Sub Category updated successfully",
                'alert-type'=>'success'
            );

            return redirect('admin/subcategory')->with($notification);
        }
        catch(Exception $e){
            $notification=array(
                'message'=>"Sub Category doesn't updated",
                'alert-type'=>'error'
            );

            return Redirect()->back()->with($notification);

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = SubCategory::where('id', $id)->first();
        $subcategory->delete();
        $notification=array(
            'message'=>"Sub category deleted",
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
