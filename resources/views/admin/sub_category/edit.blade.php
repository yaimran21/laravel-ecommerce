@extends('admin.admin_layouts')
@section('title', 'Category Create')
@section('admin_content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title  text-center m-t-0 m-b-30"> Update Category</h4>
                <form method="post" action="{{ route('subcategory.update', $subcategory->id) }}">
                    @csrf
                    @method('PUT')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                            <label for="category_id">Category</label>
                            <input type="text" name="" class="form-control" id="category_id" value="{{ $subcategory->category->name}}" disabled>
                            <input type="hidden" name="category_id" class="form-control" id="category_id" value="{{ $subcategory->category->id}}">
                    </div>
                    <div class="form-group">
                        <label for="name">Sub Category Name</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $subcategory->name }}">
                    </div>

                    <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                </form>
            </div>
        </div><!-- end col -->
    </div>
@endsection
