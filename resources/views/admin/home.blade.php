@extends('admin.admin_layouts')
@section('title', 'Home')
@section('admin_content')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-5 m-b-20">
                <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>
            <h4 class="page-title">Masonry</h4>
        </div>
    </div>

    <div class="row">
        <div class="masonry-container">
            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0"><b>Your title here</b></h4>
                    <p class="text-muted m-b-20 font-13">
                        Your awesome text goes here. Your awesome text goes here.
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-default panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Default</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-border panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Custom</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <img src="{{ asset('backend/images/thumbnail/1.jpg') }}" class="img-responsive" alt="work-thumbnail">
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <img src="{{ asset('backend/images/thumbnail/3.jpg') }}" class="img-responsive" alt="work-thumbnail">
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0"><b>Your title here</b></h4>
                    <p class="text-muted m-b-20 font-13">
                        Your awesome text goes here. Your awesome text goes here.
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-border panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Custom</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-success panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Default</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0"><b>Your title here</b></h4>
                    <p class="text-muted m-b-20 font-13">
                        Your awesome text goes here. Your awesome text goes here.
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-default panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Default</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-border panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Custom</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <img src="{{ asset('backend/images/thumbnail/2.jpg') }}" class="img-responsive" alt="work-thumbnail">
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <img src="{{ asset('backend/images/thumbnail/4.jpg') }}" class="img-responsive" alt="work-thumbnail">
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="card-box">
                    <h4 class="header-title m-t-0"><b>Your title here</b></h4>
                    <p class="text-muted m-b-20 font-13">
                        Your awesome text goes here. Your awesome text goes here.
                    </p>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-border panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Custom</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 col-md-4">
                <div class="panel panel-purple panel-border">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Default</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>
            </div>


        </div>
    </div> <!-- End row -->
@endsection
