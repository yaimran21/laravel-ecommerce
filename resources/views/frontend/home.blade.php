@extends('layouts.app')
@section('title', 'হোম')

@section('home_content')
      <!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="preloader">
    <div class="loader-inner ball-scale-multiple">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>


<?php $main_banner_category = \App\Category::findOrFail(1);
$products = $main_banner_category->products;
$i = 0;
?>
<!--/.preloader-->
<div class="slider-area slider-area--organic">

    <div data-scroll-index="0" id="rev_slider_1052_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="web-product-dark122" data-source="gallery">
        <!-- START REVOLUTION SLIDER 5.3.0.2 fullscreen mode -->
        <div id="rev_slider_1052_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.3.0.2">

            <ul>	<!-- SLIDE  -->
                @foreach($products as $product)

                    <?php $i++;?>
                    @if($i==2)
                        <li data-index="rs-2946" data-transition="fade" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1500"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="0{{$i }}" >
                            <!-- MAIN IMAGE -->
                            <img src="{{url('media/products/large',$product->image)}}" alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS 1-->
                            <div class="tp-caption tp-resizeme"
                                 id="slide-2946-layer-7"
                                 data-x="['middle','middle','middle','middle']"
                                 data-hoffset="['-0','0',0,'0']"
                                 data-y="['middle','middle','middle','middle']"
                                 data-voffset="['-220','-180','-180','-180']"
                                 data-fontsize="['150','90','80','60']"
                                 data-lineheight="['110','110','70','50']"
                                 data-width="['none','none','none','none']"
                                 data-height="none"
                                 data-whitespace="wrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                            ><span class="slider-title bangla-font"> দেশের </span></div>
                            <!-- LAYERS 2-->
                            <div class="tp-caption tp-resizeme"
                                 id="slide-2946-layer-8"
                                 data-x="['middle','middle','middle','middle']"
                                 data-hoffset="['0','0',0,'0']"
                                 data-y="['middle','middle','middle','middle']"
                                 data-voffset="['-100','-100','-110','-110']"
                                 data-fontsize="['150','90','80','60']"
                                 data-lineheight="['110','110','70','50']"
                                 data-width="['none','none','none','none']"
                                 data-height="none"
                                 data-whitespace="wrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1250,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                            ><span class="slider-title bangla-font"> ১ নাম্বার চা  </span>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption  tp-resizeme sliderপাতা-button"
                                 id="slide-2946-layer-8"
                                 data-x="['middle','middle','middle','middle']"
                                 data-hoffset="['0','0',0,'0']"
                                 data-y="['middle','middle','middle','middle']"
                                 data-voffset="['30','245','155','155']"
                                 data-width="['710','710','none','none']"
                                 data-height="['48','48','48','48']"
                                 data-whitespace="nowrap"
                                 data-type="button"
                                 data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-2947","delay":""}]'
                                 data-responsive_offset="on"
                                 data-responsive="off"
                                 data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1750,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
                                 data-textAlign="['left','left','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]" >
                                <a href="#" class="btn active bangla-font">কিনুন</a>
                            </div>
                    @elseif($i==1)
                        <li data-index="rs-2976" data-transition="fade" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1500"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="0{{$i}}" >
                            <!-- MAIN IMAGE -->
                            <img src="{{url('media/products/large',$product->image)}}" alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS 1-->
                            <div class="tp-caption tp-resizeme"
                                 id="slide-2946-layer-7"
                                 data-x="['middle','middle','middle','middle']"
                                 data-hoffset="['-0','0',0,'0']"
                                 data-y="['middle','middle','middle','middle']"
                                 data-voffset="['-220','-180','-180','-180']"
                                 data-fontsize="['150','90','80','60']"
                                 data-lineheight="['110','110','70','50']"
                                 data-width="['none','none','none','none']"
                                 data-height="none"
                                 data-whitespace="wrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                            ><span class="slider-title bangla-font"> দেশ</span></div>
                            <!-- LAYERS 2-->
                            <div class="tp-caption tp-resizeme"
                                 id="slide-2946-layer-8"
                                 data-x="['middle','middle','middle','middle']"
                                 data-hoffset="['0','0',0,'0']"
                                 data-y="['middle','middle','middle','middle']"
                                 data-voffset="['-100','-100','-110','-110']"
                                 data-fontsize="['150','90','80','60']"
                                 data-lineheight="['110','110','70','50']"
                                 data-width="['none','none','none','none']"
                                 data-height="none"
                                 data-whitespace="wrap"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1250,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]"
                            ><span class="slider-title bangla-font">  সেরা মধু </span>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption  tp-resizeme slider-button"
                                 id="slide-2946-layer-8"
                                 data-x="['middle','middle','middle','middle']"
                                 data-hoffset="['0','0',0,'0']"
                                 data-y="['middle','middle','middle','middle']"
                                 data-voffset="['30','245','155','155']"
                                 data-width="['710','710','none','none']"
                                 data-height="['48','48','48','48']"
                                 data-whitespace="nowrap"
                                 data-type="button"
                                 data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-2947","delay":""}]'
                                 data-responsive_offset="on"
                                 data-responsive="off"
                                 data-frames='[{"from":"x:-50px;opacity:0;","speed":1000,"to":"o:1;","delay":1750,"ease":"Power2.easeOut"},{"delay":"wait","speed":1500,"to":"opacity:0;","ease":"Power4.easeIn"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
                                 data-textAlign="['left','left','center','center']"
                                 data-paddingtop="[0,0,0,0]"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0]"
                                 data-paddingleft="[0,0,0,0]" >
                                <a href="#" class="btn active bangla-font">কিনুন</a>
                            </div>
                            @elseif($i >=3)
                                @break
                            @endif
                        </li>
                        @endforeach

            </ul>

            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
    </div><!-- END REVOLUTION SLIDER -->
</div>
<section class="feature-section feature-section--furniture pdt50 mt5">
    <div class="container">
        <div class="row row-eq-rs-height">
            <?php
            $categories = DB::table('categories')
                ->whereIn('id', [2,3])
                ->get();
            ?>

            @foreach($categories as $category)
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="organic-feature">
                        <div class="organic-feature__text-content">
                            <h3 class="organic-feature__title bangla-font"><a href="#"> {{ $category->name }}</a> </h3>
                        </div>
                        <div class="organic-feature__image">
                            <img src="{{URL::to('/media/category/large/',$category->logo) }}" alt="">
                        </div>
                    </div><!--/.organic-feature-->
                </div>
            @endforeach
            <div/>
        </div>
    </div>
</section>
<section class="pdt35">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading2 text-center no-margin">
                    <h2 class="section-title bangla-font">ফিচার্ড প্রোডাক্ট</h2>
                    <img src="{{asset('frontend/images/organic/separator.png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="filter-options mb45 bangla-font  mt10">
                    <ul>
                        <li><a href="#!" class="filter" data-filter=".all"> সব</a></li>
                        <li><a href="#!" class="filter" data-filter=".honey">মধু</a></li>
                        <li><a href="#!" class="filter" data-filter=".tea">চা</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row magnificPopup-wrapper" id="mixitup-grid">
            <?php $honey_featured_category = \App\Category::findOrFail(2);
            $honey_featured_products = $honey_featured_category->products;
            $tea_category = \App\Category::findOrFail(3);
            $tea_featured_products = $tea_category->products;
            $i = 0;
            $increment = 0;
            ?>


            @foreach( $honey_featured_products as $honey_featured_product)
                <?php $i ++; ?>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 mix honey all">
                    <div class="product-item product-item--border">
                        <div class="product-item__image-wrap">
                            <span class="discount-percent on-sale discount-percent--green"><span>Sale</span></span>
                            <img class="product-item__image" src="{{URL::to('media/products/small/', $honey_featured_product->image)}}" alt="{{ $honey_featured_product->title }}">
                            <div class="product-item__actions">
                                <a href="#product-popup-honey-{{$i}}" class="product-item__action magnific-popup-active" data-effect="mfp-zoom-in" data-toggle="tooltip" data-placement="top" title=" Quick Watch"><i class="icon-icomoon-view"></i></a>
                                <a href="javascript:;" class="product-item__action add-to-cart" data-id="{{$honey_featured_product->id}}" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="icon-icomoon-shopping-cart2"></i></a>
                                <a href="javascript:;" class="product-item__action add-wish-list" data-id="{{$honey_featured_product->id}}" data-toggle="tooltip" data-placement="top" title="Add To Favorite"><i class="icon-icomoon-heart"></i></a>
                            </div>
                        </div>
                        <div class="product-item__text-content">
                            <h5 class="product-item__title"><a href="#"> {{ $honey_featured_product->title }}</a></h5>
                            <div class="product-rating">
                                <input type="hidden" value="5" class="rating"/>
                            </div>
                            {{--                                <span class="featre-item__price">{{ $honey_featured_product->price }}</span>--}}
                            {{--                                <span class="featre-item__price"><s class="pdr5">{{ $honey_featured_product->sale_price }}</s> {{ $honey_featured_product->price }}</span>--}}
                            <span class="featre-item__price">{{ $honey_featured_product->price }}</span>
                        </div>
                        <!-- Popup itself -->
                        <div id="product-popup-honey-{{$i}}" class="quickview-wrapper mfp-with-anim mfp-hide">
                            <div class="quickview">
                                <div class="image-wrap">
                                    <img src="{{URL::to('media/products/large', $honey_featured_product->image)}}" alt="{{ $honey_featured_product->title }}">
                                </div>
                                <div class="single-product">
                                    <h2 class="single-product__title"> {{ $honey_featured_product->title }}</h2>
                                    <div class="product-rating mb10">
                                        <input type="hidden" class="rating" value="5" readonly/>
                                    </div>
                                    <span class="single-product__price pdb15 disinb">{{ $honey_featured_product->price }}</span>
                                    <p>{!! $honey_featured_product->details !!}</p>
                                    <div class="single-product-cart pdt10 pdb45">
                                        <div class="item-quantity mr10">
                                            <i class="fa fa-minus decrement-button"></i>
                                            <input type="text" class="product-quantity" value="1"/>
                                            <i class="fa fa-plus increment-button"></i>
                                        </div>
                                        <a class="add-to-cart" data-id="{{ $honey_featured_product->id }}">
                                            <button type="button" class="btn mr10">add to cart</button>
                                        </a>
                                        <a class="fav-button add-wish-list" href="javascript:;" data-id="{{$honey_featured_product->id}}" data-toggle="tooltip" data-placement="top" title="Add To Favorite" ><i class="icon-icomoon-heart"></i> </a>
                                    </div>
                                    <div class="single-product__categories pdb20">
                                        <span class="title">Category : </span>
                                        <div class="single-product__category">
                                            @foreach($honey_featured_product->categories as $category)
                                                <a href="#"> {{ $category->name }}</a>
                                            @endforeach
                                        </div>

                                    </div>
                                    <div class="single-product__colors pdb20">
                                        <span class="title">Code :</span>
                                        <div class="single-product__category">
                                            <p>{{ $honey_featured_product->sku }}</p>
                                        </div>

                                        {{--                                        <div class="single-product__color">--}}
                                        {{--                                            <div class="radio-button disinb">--}}
                                        {{--                                                <input  class="radio-button__input" type="radio" name="color" value="1" id="color-green-1" checked/>--}}
                                        {{--                                                <label class="radio-button__label radio-button__label--green" for="color-green-1">Green</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="radio-button disinb">--}}
                                        {{--                                                <input  class="radio-button__input" type="radio" name="color" value="2" id="color-red-1"/>--}}
                                        {{--                                                <label class="radio-button__label radio-button__label--red" for="color-red-1">Red</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    <hr/>
                                    <div class="product-share-social">
                                        <span class="bold-title pdr15">Social Share : </span>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div><!--/.single-product-->
                            </div>
                        </div>
                    </div><!--/.product-item-->
                </div>
            @endforeach

            @foreach( $tea_featured_products as $tea_featured_product)
                <?php $increment ++; ?>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 mix tea all">
                    <div class="product-item product-item--border">
                        <div class="product-item__image-wrap">
                            <span class="discount-percent on-sale discount-percent--green"><span>Sale</span></span>
                            <img class="product-item__image" src="{{URL::to('media/products/small/', $tea_featured_product->image)}}" alt="{{ $tea_featured_product->title }}">
                            <div class="product-item__actions">
                                <a href="#product-popup-tea-{{$increment}}" class="product-item__action magnific-popup-active" data-effect="mfp-zoom-in" data-toggle="tooltip" data-placement="top" title=" Quick Watch"><i class="icon-icomoon-view"></i></a>
                                <a href="javascript:;" class="product-item__action add-to-cart" data-id="{{ $tea_featured_product->id }}"  data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="icon-icomoon-shopping-cart2"></i></a>
                                <a href="javascript:;" class="product-item__action add-wish-list" data-id="{{ $tea_featured_product->id }}" data-toggle="tooltip" data-placement="top" title="Add To Favorite"><i class="icon-icomoon-heart"></i></a>
                            </div>
                        </div>
                        <div class="product-item__text-content">
                            <h5 class="product-item__title"><a href="#"> {{ $tea_featured_product->title }}</a></h5>
                            <div class="product-rating">
                                <input type="hidden" value="5" class="rating"/>
                            </div>
                            <span class="featre-item__price">{{ $tea_featured_product->price }}</span>
                        </div>
                        <!-- Popup itself -->
                        <div id="product-popup-tea-{{$increment}}" class="quickview-wrapper mfp-with-anim mfp-hide">
                            <div class="quickview">
                                <div class="image-wrap">
                                    <img src="{{URL::to('media/products/large', $tea_featured_product->image)}}" alt="{{ $tea_featured_product->title }}">
                                </div>
                                <div class="single-product">
                                    <h2 class="single-product__title"> {{ $tea_featured_product->title }}</h2>
                                    <div class="product-rating mb10">
                                        <input type="hidden" class="rating" value="5" readonly/>
                                    </div>
                                    <span class="single-product__price pdb15 disinb">{{ $tea_featured_product->price }}</span>
                                    <p>{!! $tea_featured_product->details !!}</p>
                                    <div class="single-product-cart pdt10 pdb45">
                                        <div class="item-quantity mr10">
                                            <i class="fa fa-minus decrement-button"></i>
                                            <input type="text" class="product-quantity" value="1"/>
                                            <i class="fa fa-plus increment-button"></i>
                                        </div>


                                        <a class="add-to-cart" data-id="{{ $tea_featured_product->id }}">
                                            <button type="button" class="btn mr10">add to cart</button>
                                        </a>
                                        <a class="fav-button add-wish-list" href="javascript:;" data-id="{{$tea_featured_product->id}}" data-toggle="tooltip" data-placement="top" title="Add To Favorite" ><i class="icon-icomoon-heart"></i> </a>
                                    </div>
                                    <div class="single-product__categories pdb20">
                                        <span class="title">Category : </span>
                                        <div class="single-product__category">
                                            @foreach($tea_featured_product->categories as $category)
                                                <a href="#"> {{ $category->name }}</a>
                                            @endforeach
                                        </div>

                                    </div>
                                    <div class="single-product__colors pdb20">
                                        <span class="title">Code :</span>
                                        <div class="single-product__category">
                                            <p>{{ $tea_featured_product->sku }}</p>
                                        </div>

                                        {{--                                        <div class="single-product__color">--}}
                                        {{--                                            <div class="radio-button disinb">--}}
                                        {{--                                                <input  class="radio-button__input" type="radio" name="color" value="1" id="color-green-1" checked/>--}}
                                        {{--                                                <label class="radio-button__label radio-button__label--green" for="color-green-1">Green</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="radio-button disinb">--}}
                                        {{--                                                <input  class="radio-button__input" type="radio" name="color" value="2" id="color-red-1"/>--}}
                                        {{--                                                <label class="radio-button__label radio-button__label--red" for="color-red-1">Red</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                    <hr/>
                                    <div class="product-share-social">
                                        <span class="bold-title pdr15">Social Share : </span>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div><!--/.single-product-->
                            </div>
                        </div>
                    </div><!--/.product-item-->
                </div>
            @endforeach




        </div>
    </div>
</section>
<div class="discount-section">
    <div class="container">
        <div class="text-center">
            <h1 class="discount-title discount-title--gradient">Discount</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="discount-carousel owl owl-carousel"  data-itemsDesktop="1" data-itemsDesktopSmall="1" data-itemsTablet="1" data-itemsMobile="1" data-autoplay="true" data-pagination="false" data-margin="0" data-navigation="true">
                    <div class="discount-carousel-item discount-carousel-item--organic">
                        <div class="text-center discount-carousel-item__details">
                            <img class="separator" src="{{asset('frontend/images/organic/separator.png')}}" alt="">
                            <h3 class="discount-carousel-item__title"><a href="#">Fruit Package</a></h3>
                            <div class="price">
                                <span class="base-color">$45.00</span>
                                <small>$60.00</small>
                            </div>
                            <div class="counter-wrap pdt15 pdb35">
                                <div class="counter-active" data-year="2018" data-month="9" data-date="28">
                                </div>
                            </div>
                            <div class="discount-carousel-item__image">
                                <div class="pr">
                                    <span class="discount-percent"><span>%50</span></span>
                                </div>
                                <img src="{{'frontend/images/organic/discount1.png'}}" alt="">
                            </div>
                        </div>
                    </div><!--/.discount-carousel-item-->
                    <div class="discount-carousel-item discount-carousel-item--two">
                        <div class="text-center discount-carousel-item__details">
                            <img class="separator" src="{{asset('frontend/images/organic/separator.png')}}" alt="">
                            <h3 class="discount-carousel-item__title"><a href="#">Big Antique fruit</a></h3>
                            <div class="price">
                                <span class="base-color">$45.00</span>
                                <small>$60.00</small>
                            </div>
                            <div class="counter-wrap pdt15 pdb35">
                                <div class="counter-active" data-year="2018" data-month="9" data-date="28">
                                </div>
                            </div>
                            <div class="discount-carousel-item__image">
                                <div class="pr">
                                    <span class="discount-percent"><span>%50</span></span>
                                </div>
                                <img src="{{asset('frontend/images/organic/discount2.png')}}" alt="">
                            </div>
                        </div>
                    </div><!--/.discount-carousel-item-->
                </div>
            </div>
        </div>
    </div>
</div>
<section class="pdt40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading2 text-center">
                    <h2 class="section-title">Blog</h2>
                    <img src="{{asset('frontend/images/organic/separator.png')}}" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                <div class="blog-post">
                    <div class="blog-post__image-wrap">
                        <img src="{{asset('frontend/images/organic/blog/blog1.jpg')}}" alt="">
                        <div class="post-date">
                            <span class="date">25</span>
                            <span class="month">May</span>
                        </div>
                    </div>
                    <div class="blog-post__text-content">
                        <h3 class="blog-post__title"><a href="#">Be a trendsetter</a></h3>
                        <p>Duis aute irure dolor in reprehen derit amo oluptate velit esse</p>
                        <a href="#" class="blog-post__read-more">Read More</a>
                    </div>
                </div><!--/.blog-post-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                <div class="blog-post">
                    <div class="blog-post__image-wrap">
                        <img src="{{asset('frontend/images/organic/blog/blog2.jpg')}}" alt="">
                        <div class="post-date">
                            <span class="date">25</span>
                            <span class="month">May</span>
                        </div>
                    </div>
                    <div class="blog-post__text-content">
                        <h3 class="blog-post__title"><a href="#">Fashion is beauty</a></h3>
                        <p>Duis aute irure dolor in reprehen derit amo oluptate velit esse</p>
                        <a href="#" class="blog-post__read-more">Read More</a>
                    </div>
                </div><!--/.blog-post-->
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                <div class="blog-post">
                    <div class="blog-post__image-wrap">
                        <img src="{{asset('frontend/images/organic/blog/blog3.jpg')}}" alt="">
                        <div class="post-date">
                            <span class="date">25</span>
                            <span class="month">May</span>
                        </div>
                    </div>
                    <div class="blog-post__text-content">
                        <h3 class="blog-post__title"><a href="#">Long way to go</a></h3>
                        <p>Duis aute irure dolor in reprehen derit amo oluptate velit esse</p>
                        <a href="#" class="blog-post__read-more">Read More</a>
                    </div>
                </div><!--/.blog-post-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center pdb30">
                <a href="#" class="btn">View all</a>
            </div>
        </div>
    </div>
</section>
<div class="client-area-organic mt50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="clients-carousel owl owl-carousel"  data-itemsDesktop="6" data-itemsDesktopSmall="4" data-itemsTablet="3" data-itemsMobile="2" data-autoplay="true" data-pagination="false" data-margin="0" data-navigation="false">
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client1.png')}}" alt="">
                    </div>
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client2.png')}}" alt="">
                    </div>
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client3.png')}}" alt="">
                    </div>
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client4.png')}}" alt="">
                    </div>
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client5.png')}}" alt="">
                    </div>
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client6.png')}}" alt="">
                    </div>
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client1.png')}}" alt="">
                    </div>
                    <div class="client-logo-wrap">
                        <img src="{{asset('frontend/images/fashion/client2.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('frontend_one_page_js')

    {{--    for wishlist--}}
    <script>
        $(document).ready(function () {
            $('.add-wish-list').on('click', function (){
                var product_id = $(this).data('id');
                console.log(product_id);
                if(product_id){
                    $.ajax({
                        url: "{{ url('wishlist/add/')}}/"+product_id,
                        type: "GET",
                        dataType:'json',
                        success:function (data){
                            swal({
                                title: data['title'],
                                // text: "You added product to wishlist!",
                                text: data['message'],
                                icon: data['icon'],
                            });
                        },
                        error:function (data) {
                            swal({
                                title:data['title'],
                                text: data['message'],
                                icon: data['icon'],
                            });
                        }
                    });
                }

                else {
                    alert('danger')
                }
            });
        });
    </script>


    {{--For add to cart--}}
    <script>
        $(document).ready(function () {
            $('.add-to-cart').on('click', function (){
                var product_id = $(this).data('id');
                if(product_id){
                    $.ajax({
                        url: "{{ url('cart/add/')}}/"+product_id,
                        type: "GET",
                        dataType:'json',
                        success:function (data){
                            swal({
                                title: data['title'],
                                text: data['message'],
                                icon: data['icon'],
                            });
                        },
                        error:function (data) {
                            alert('have some problem');

                            // swal({
                            //     title:data['title'],
                            //     text: data['message'],
                            //     icon: data['icon'],
                            // });
                        }
                    });
                }

                else {
                    alert('danger')
                }
            });
        });
    </script>
@endsection
