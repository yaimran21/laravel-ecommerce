@extends('admin.admin_layouts')
@section('title', 'Brand Create')
@section('admin_content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title  text-center m-t-0 m-b-30"> Add Brand</h4>
                <form method="post" action="{{ route('brand.store') }}" enctype="multipart/form-data">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Enter brand name...">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="filer_input">Brand Logo</label>
                                <input type="file" name="logo" class="form-control" id="filer_input">
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                </form>
            </div>
        </div><!-- end col -->
    </div>
@endsection


@section('on_page_js')
    <script>
        $(document).ready(function() {
            $('#filer_input').filer( {
                    limit: 1,
                    maxSize: 3,
                    extensions: ["jpg", "png", "gif"],
                    showThumbs: true
                }
            );
        });
    </script>

@endsection
