<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='orders';
    protected $primaryKey='id';
    protected $fillable=['user_id',
        'user_email','user_name','address','city','state','zipcode',
        'country','mobile','shipping_charges',
        'order_status','payment_method','grand_total'];
}
