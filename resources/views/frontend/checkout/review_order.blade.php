@extends('layouts.app')
@section('title', 'কার্ট')

@section('home_content')
    <!-- Start content -->
    <div class="container">
        <br>
        <br>
        <br>
        <div class="row card-box">
            <form action="{{ route('checkout.order') }}" method="post" class="form-horizontal">
                @csrf
                {{--            <form action="{{url('/submit-order')}}" method="post" class="form-horizontal">--}}
                <div class="col-sm-12">
                    <input type="hidden" name="user_id" value="{{$shipping_address->user_id}}">
                    <input type="hidden" name="user_email" value="{{$shipping_address->user_email}}">
                    <input type="hidden" name="user_name" value="{{$shipping_address->user_name}}">
                    <input type="hidden" name="address" value="{{$shipping_address->address}}">
                    <input type="hidden" name="city" value="{{$shipping_address->city}}">
                    <input type="hidden" name="state" value="{{$shipping_address->state}}">
                    <input type="hidden" name="zipcode" value="{{$shipping_address->zipcode}}">
                    <input type="hidden" name="country" value="{{$shipping_address->country}}">
                    <input type="hidden" name="mobile" value="{{$shipping_address->mobile}}">
                    <input type="hidden" name="shipping_charges" value="0">
                    <input type="hidden" name="order_status" value="success">
                    {{--                        @if(Session::has('discount_amount_price'))--}}
                    {{--                            <input type="hidden" name="coupon_code" value="{{Session::get('coupon_code')}}">--}}
                    {{--                            <input type="hidden" name="coupon_amount" value="{{Session::get('discount_amount_price')}}">--}}
                    {{--                            <input type="hidden" name="grand_total" value="{{$total_price-Session::get('discount_amount_price')}}">--}}
                    {{--                        @else--}}
                    {{--                            <input type="hidden" name="coupon_code" value="NO Coupon">--}}
                    {{--                            <input type="hidden" name="coupon_amount" value="0">--}}
                    {{--                            <input type="hidden" name="grand_total" value="{{$total_price}}">--}}
                    {{--                        @endif--}}

                    <div class="col-sm-12">
                        <h3 class="heading">Shipping To</h3>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Zip Code</th>
                                    <th>Mobile</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$shipping_address->user_name}}</td>
                                    <td>{{$shipping_address->address}}</td>
                                    <td>{{$shipping_address->city}}</td>
                                    <td>{{$shipping_address->state}}</td>
                                    <td>{{$shipping_address->country}}</td>
                                    <td>{{$shipping_address->zipcode}}</td>
                                    <td>{{$shipping_address->mobile}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <br> <br>
                        <h4 class="header-title m-t-0 m-b-30">Review & Payment</h4>
                        <table class="table table table-hover m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Product Title</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cart_items as $c_iteam)
                                <?php
                                $i++; ?>
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td><img style="height: 30px; width: 30px;" src="{{ url('media/products/small',$c_iteam->options->image) }}" alt=""></td>
                                    <td>{{ $c_iteam->name }}</td>
                                    {{--                                <td>{{ $c_iteam->qty }}</td>--}}
                                    <td>{{$c_iteam->qty}}</td>
                                    <td>{{ $c_iteam->price }}</td>
                                    <td>{{ $c_iteam->price*$c_iteam->qty }}</td>
                                    <td>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table class="table table m-0">
                            <thead>
                            <th></th>
                            </thead>

                            <tbody>
                            <td align="right">Total: <b>{{ Cart::total() }}</b> Tk</td>

                            </tbody>
                        </table>
                        <table class="table table m-0">
                            <thead>
                            </thead>

                            <tbody>
                            <td align="left">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0">Delivery Method</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="payment_method" value="COD" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                Cash On Delivery
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="payment_method" id="gridRadios2" value="paypal">
                                            <label class="form-check-label" for="gridRadios2">
                                                Paypal
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td align="right">
                                <a class="" href="{{ route('checkout') }}">
                                    <button class="btn btn btn-success">Order Now</button>
                                </a>
                            </td>

                            </tbody>
                        </table>
                    </div>
                </div><!-- end col -->
            </form>
        </div>
    </div> <!-- container -->
@endsection
