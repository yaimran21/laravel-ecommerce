<?php

namespace App\Http\Controllers;

use App\WishList;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use PhpParser\Node\Stmt\Return_;

class WishListController extends Controller
{
    public function add_wishlist($product_id){

        $user_id = Auth::id();
        $check_wishlist = DB::table('wish_lists')->where('user_id', $user_id)->where('product_id', $product_id)->first();
        $data = array();
        $data['user_id'] = $user_id;
        $data['product_id'] = $product_id;

        if (Auth::check()){
            if ($check_wishlist){
                return response()->json(['message'=>'Already in your wishlist', 'icon'=> 'warning', 'title'=> 'Opps!']);
            }
            else{
                DB::table('wish_lists')->insert($data);
                return response()->json(['message'=>'Added to Wishlist', 'icon'=> 'success', 'title'=> 'Yeah!']);
            }

        }
        else{
            return response()->json(['message'=>"Please login first to add product to cart", 'icon'=> 'error', 'title'=> 'Oh no!']);
        }

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
