<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;




class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN_HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function admin_login(){

        return view('admin.auth.login');
    }

//    public function login(Request $request)
//    {
//        $this->validate($request, [
//            'email' => 'required|string',
//            'password' => 'required|string',
//        ]);
//
//
//        $credential = [
//            'email'=>$request->email,
//            'password'=>$request->password,
//        ];
//
//        if(Auth::guard('admin')->attempt($credential, $request->member)){
//            return redirect()->intended(route('admin.home'));
//        }
//
//        return redirect()->back()->withInput($request->only('email,remember'));
//    }

//    public function showLoginForm()
//    {
//         if (Auth::guard('admin')) {
//             return redirect('admin/home');
//         }
//         else{
//            return view('admin.auth.login');
//         }
//
//    }

    protected function guard()
    {
        return Auth::guard('admin');
    }


}
