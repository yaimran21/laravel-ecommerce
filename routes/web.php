<?php

Route::get('/', function () {return view('frontend.home');});
//auth & user
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/password-change', 'HomeController@changePassword')->name('password.change');
Route::post('/password-update', 'HomeController@updatePassword')->name('password.update');
Route::get('/user/logout', 'HomeController@Logout')->name('user.logout');

//admin=======
Route::get('admin/home', 'AdminController@index')->name('admin.home');
Route::get('admin', 'Admin\LoginController@admin_login')->name('admin.login');
Route::post('admin', 'Admin\LoginController@login')->name('admin_login.store');
// Password Reset Routes...
Route::get('admin/password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin/reset/password/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
Route::post('admin/update/reset', 'Admin\ResetPasswordController@reset')->name('admin.reset.update');
Route::get('/admin/Change/Password','AdminController@ChangePassword')->name('admin.password.change');
Route::post('/admin/password/update','AdminController@Update_pass')->name('admin.password.update');
Route::get('admin/logout', 'AdminController@logout')->name('admin.logout');

// Admin routes


//For general user
Route::get('wishlist/add/{product_id}', 'WishListController@add_wishlist')->name('wishlist.add');
Route::get('cart/add/{product_id}', 'CartController@add_cart')->name('cart.add');
Route::get('product/view-cart/', 'CartController@index')->name('cart.index');
Route::get('cart/delete/{rowId}', 'CartController@destroy')->name('cart.remove');
Route::post('cart/update', 'CartController@update')->name('cart.update');
Route::get('cart/check', 'CartController@check')->name('cart.check');
Route::get('product/checkout', 'CheckOutController@checkout')->name('checkout');
Route::post('product/checkout/store', 'CheckOutController@store')->name('checkout.store');
Route::get('product/checkout/review', 'CheckOutController@review_order')->name('checkout.review');
Route::post('product/submit-order','OrderController@order')->name('checkout.order');
Route::get('product/order/cod','OrderController@cod')->name('checkout.cod');
Route::get('product/order/paypal','OrderController@paypal')->name('checkout.paypal');
//Route::get('product/checkout', 'CartController@checkout')->name('checkout');

//For admin user
// Category
Route::prefix('admin')->group(function () {
    Route::get('category/list', 'CategoryController@index')->name('category.index');
    Route::get('category/create', 'CategoryController@create')->name('category.create');
    Route::post('store/category', 'CategoryController@store')->name('category.store');
    Route::post('category/{id}', 'CategoryController@delete')->name('category.delete');
    Route::get('category/edit/{id}', 'CategoryController@edit')->name('category.edit');
    Route::post('category/update/{id}', 'CategoryController@update')->name('category.update');
// Brand
    Route::resource('brand', 'BrandController');
// Sub Category
    Route::resource('subcategory', 'SubCategoryController');
// Product
    Route::resource('product', 'ProductController');
    Route::get('delete-image/{id}','ProductController@deleteImage');
// Coupon
    Route::resource('coupon', 'CouponController');
/// Product Attribute
    Route::resource('attribute','ProductAttributeController');
//Route::get('delete-attribute/{id}','ProductAtrrController@deleteAttr');
/// Product Images Gallery
    Route::resource('/imagegallery','ImageGalleryController');
//Route::get('delete-imageGallery/{id}','ImagesController@destroy');
//    Route::resource('wishlist', 'WishListController');
});


