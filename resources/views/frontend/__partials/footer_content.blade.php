<footer class="xara-footer xara-footer--2 xara-footer--fashion">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-widget__title">About Us</h3>
                        <p class="text-justify">when our power of choice is untram melled ances and owing the claims of duty oron oblig business amon</p>
                        <div class="pdb5">Email: email@website.com</div>
                        <div>Phone: +1 325 620 6935</div>
                    </div><!--/.footer-widget-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-widget__title">Useful Links</h3>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Support Center</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Order Tracking</a></li>
                        </ul>
                    </div><!--/.footer-widget-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-widget__title">Useful Links</h3>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Support Center</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Order Tracking</a></li>
                        </ul>
                    </div><!--/.footer-widget-->
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-widget">
                        <h3 class="footer-widget__title">Newsletter</h3>
                        <p>Enter your email for keep update with us.</p>
{{--                        <form action="{{ route('newsletter.store') }}">--}}
                        <form action="#">
                            <div class="newsletter-form">
                                <input type="email" name="email" class="form-control newsletter-form__input" placeholder="Enter your email"/>
                                <button type="submit" class="newsletter-form__submit"><i class="icon-icomoon-right-arrow base-gradient-color"></i> </button>
                            </div>
                        </form>
                    </div><!--/.footer-widget-->
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-6">
                    <div class="footer-logo-wrap">
                        <a href="#" class="footer-logo"><img src="{{asset('frontend/images/organic/logo.png')}}" alt=""></a>
                        <span>&copy; Themebeer</span>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <div class="text-right pdt5">
                        <ul class="social-icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
