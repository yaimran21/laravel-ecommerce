@extends('admin.admin_layouts')
@section('title', 'Category index')
@section('admin_content')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <h4 style="display: inline-block" class="header-title m-t-0 m-b-30">Category Index</h4>
                {{--                <button   type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light " data-toggle="modal" data-target="#category-create-modal"> Create New</button>--}}
                <a href="{{ route('category.create') }}">
                    <button type="button" class="btn btn-info btn-rounded w-md waves-effect waves-light " > Create New</button>

                </a>


                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                    <tr>
                        <th>Id#</th>
                        <th>Name</th>
                        <th>Parent Category</th>
                        <th>Created At</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($categories as $category)
                        <?php $id++; ?>
                        <?php
                        $parent_categories = DB::table('categories')->select('name')->where('id', $category->parent_id)->get();
                        ?>
                        <tr>
                            <td>{{ $id }}</td>
                            <td>{{ $category->name }}</td>
                                <td>
                                    @foreach($parent_categories as $parent_category)

                                    {{$parent_category->name ?? 'No parent category'}}
                                    @endforeach

                                </td>





                            <td>{{ $category->created_at->diffForHumans() }}</td>
                            <td>{{($category->status==0)?' Inactive':'Active'}}</td>

                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('category.edit', $category->id) }} ">
                                    <i class="fa fa-edit">
                                    </i>
                                </a>

                                <a href="#" class="btn btn-danger btn-xs delete-modal" data-id="{{ $category->id }}" data-toggle="modal" data-target=".category_delete_modal">
                                    <i id="categoryName" data-id="{{ $category->name }}" class="fa fa-trash">
                                    </i>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    {{--    <div id="category-create-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">--}}
    {{--        <div class="modal-dialog">--}}
    {{--            <form method="post" action="{{ route('category.store') }}">--}}
    {{--                @csrf--}}
    {{--                <div class="modal-content">--}}
    {{--                    <div class="modal-header">--}}
    {{--                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
    {{--                        <h4 class="modal-title"> Create Category</h4>--}}
    {{--                    </div>--}}
    {{--                    <div class="modal-body">--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-md-12">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label for="name" class="control-label">Name</label>--}}
    {{--                                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter category name.." required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="modal-footer">--}}
    {{--                        <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </form>--}}


    {{--        </div>--}}
    {{--    </div><!-- /.modal -->--}}


    <!-- Delete Modal -->
    <div class="modal fade category_delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="mySmallModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p id="category_name">
                    <form id="deleteForm" action="" method="post">
                        @csrf
                        <button type="submit" class="btn btn-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('on_page_js')
    <script>
        $(document).on("click", ".delete-modal", function () {
            let categoryId = $(this).data('id');
            let categoryName = $('#categoryName').data('id');
            $(".modal-body #category_name").text( "Do you want to delete "+ categoryName +' category?' );
            var route = '/admin/category/'+categoryId
            $('#deleteForm').attr('action', route)
        });

    </script>
@endsection
