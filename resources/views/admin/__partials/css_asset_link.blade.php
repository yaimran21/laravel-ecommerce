
<!-- file upload  -->

<link href="{{ asset('backend/plugins/jquery.filer/css/jquery.filer.css') }}" rel="stylesheet" />
<link href="{{ asset('backend/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet" />

<!-- DataTables -->
<link href="{{ asset('backend/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href=" {{ asset('backend/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href=" {{ asset('backend/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href=" {{ asset('backend/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Core js -->
<link rel="stylesheet" href="{{ asset('backend/plugins/morris/morris.css') }}">
<link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/css/menu.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/css/core.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/css/components.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/css/responsive.css') }}" rel="stylesheet" type="text/css" />

<script src="{{ asset('backend/js/modernizr.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-77043005-1', 'auto');
    ga('send', 'pageview');

</script>

<!-- select2, multiple select -->

<link href="{{ asset('backend/plugins/summernote/summernote.css') }}" rel="stylesheet" />
<link href="{{ asset('backend/plugins/multiselect/css/multi-select.css') }}"  rel="stylesheet" type="text/css" />
<link href="{{ asset('backend/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('backend/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('backend/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
{{--    <script src="{{ asset('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>--}}
{{--    <script src="{{ asset('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}"></script>--}}
    <![endif]-->
<!-- For tag input -->
{{--<link href="{{asset('backend/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />--}}
<!-- Summer Note Design -->
{{--<link href="{{ asset('backend/plugins/summernote/summernote.css') }}" rel="stylesheet" />--}}


<!-- Material Design -->
{{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}


