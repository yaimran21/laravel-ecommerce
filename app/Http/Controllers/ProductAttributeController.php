<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductAttribute;
use Illuminate\Http\Request;

class ProductAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'sku'=>'required',
            'size'=>'required',
            'price'=>'required|numeric',
            'stock'=>'required|numeric'
        ]);
        ProductAttribute::create($request->all());
        $notification=array(
            'message'=>"Product attr created sucessfully",
            'alert-type'=>'success'
        );
        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductAttribute  $productAttribute
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $i = 0;
        $attributes=ProductAttribute::where('product_id',$id)->get();
        $product=Product::findOrFail($id);
        return view('admin.product.add_product_attr',compact('i','product','attributes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductAttribute  $productAttribute
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductAttribute $productAttribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductAttribute  $productAttribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_data=$request->all();
        foreach ($request_data['id'] as $key=>$attr){

//            dd($request_data['sku'][$key]);
            $update_attr=ProductAttribute::where([['product_id',$id],['id',$request_data['id'][$key]]])
                ->update([
                    'sku'=>$request_data['sku'][$key],
                    'size'=>$request_data['size'][$key],
                    'price'=>$request_data['price'][$key],
                    'stock'=>$request_data['stock'][$key]
                ]);
        }
        $notification=array(
            'message'=>"Product attr updated successfully",
            'alert-type'=>'success'
        );
        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductAttribute  $productAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribute = ProductAttribute::where('id', $id)->first();
        $attribute->delete();
        $notification=array(
            'message'=>"Product attribute deleted",
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
}
