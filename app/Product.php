<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['title', 'sku', 'brand_id',
        'details', 'sale_price', 'quantity', 'price',
        'image', 'video_link', 'weight', 'color', 'status'];

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTimestamps();
//        return $this->belongsToMany(Category::class)->using(CategoryProduct::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function product_attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }


    public function check_category_in_pivot_by_id($id){
        return $this->categories()->firstWhere('category_id', $id);
    }

}
