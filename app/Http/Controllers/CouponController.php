<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin.coupon.index')->withCoupons($coupons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request){


        $validatedData = $request->validate([
            'code' => 'required|unique:coupons|max:255',
            'type' => 'required|max:255',
            'status' => 'required|max:255',
        ]);

        try{
            $coupon = new Coupon();
            $coupon->code = $request['code'];
            $coupon->type = $request['type'];
            $coupon->status = $request['status'];
            $coupon->value = $request['value'] ? $request['value']: 0.0 ;
            $coupon->percent_of = $request['percent_of'] ? $request['percent_of']: 0.0;
            $coupon->save();

            $notification=array(
                'message'=>'Coupon created successfully',
                'alert-type'=>'success'
            );
            return redirect()->route('coupon.index')->with($notification);
        }
        catch(Exception $e){
            $notification=array(
                'message'=>"Coupon doesn't created",
                'alert-type'=>'error'
            );

            return Redirect()->back()->with($notification);

        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::where('id', $id)->first();

        return view('admin.coupon.edit')->withCoupon($coupon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        try {
        $coupon = Coupon::findOrFail($id);
        $coupon->value = $request['value'] ? $request['value'] :0.0;
        $coupon->percent_of = $request['percent_of']? $request['percent_of'] :0.0;
        $coupon->status = $request['status'];

        $coupon->save();
        $notification=array(
            'message'=>"Coupon Updated",
            'alert-type'=>'success'
        );
        return redirect()->route('coupon.index')->with($notification);

        }

    catch(Exception $e){
        $notification=array(
            'message'=>"Coupon doesn't created",
            'alert-type'=>'error'
        );

        return Redirect()->back()->with($notification);

    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::where('id', $id)->first();

        $coupon->delete();
        $notification=array(
            'message'=>"Coupon deleted",
            'alert-type'=>'error'
        );

        return Redirect()->back()->with($notification);
    }
}
