@extends('layouts.app')
@section('title', 'হোম')
@section('home_content')
{{--    <div class="wrapper without_header_sidebar">--}}
        <!-- contnet wrapper -->
        <div class="container">
            <!-- page content -->

{{--            <div class="logo">--}}
{{--                <img src="{{asset('public/panel/assets/images/logo.png')}}" alt="" class="img-fluid">--}}
{{--            </div>--}}
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h5 class="card-title text-center">Customer Login</h5>
                    <form action="{{route('login')}}" class="d-block" method="post">
                        @csrf
                        <div class="form-group icon_parent">
                            <label for="email">E-mail</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email Address">
{{--                            <span class="icon_soon_bottom_right"><i class="fa fa-envelope"></i></span>--}}
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group icon_parent">
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
{{--                            <span class="icon_soon_bottom_right"><i class="fa fa-unlock"></i></span>--}}
                        </div>
                        <div class="form-group">
                            <label class="chech_container">Remember me
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group">
                            <a class="registration" href="{{route('register')}}">Create new account</a><br>
                            <a href="{{ route('password.request') }}" class="text-white">I forgot my password</a>
                            <button type="submit" class="btn btn-blue">Login</button>
                        </div>
                    </form>

                </div>
            </div>
{{--            <div class="footer">--}}
{{--                <p>Copyright &copy; 2019 <a href="https://durbarit.com/">Durbar IT</a>. All rights reserved.</p>--}}
{{--            </div>--}}
        </div><!--/ content wrapper -->
{{--    </div><!--/ wrapper -->--}}
@endsection
