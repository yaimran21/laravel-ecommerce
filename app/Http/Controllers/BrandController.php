<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use Illuminate\Http\Request;

class BrandController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        return view('admin.brand.index')->withBrands($brands);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:brands|max:255',
            'logo' => 'required'
        ]);

        try{
            $brand = new Brand();
            $brand->name = $request['name'];
            $logo = $request->file('logo');
            $logo_name = date('d_m_y_i');
            $ext = strtolower($logo->getClientOriginalExtension());
            $logo_full_name = $logo_name. '.' . $ext;
            $upload_path = 'media/brand/';
            $logo_url = $upload_path.$logo_full_name;
            $logo->move($upload_path, $logo_full_name);

            $brand->logo =  $logo_url;
            $brand->save();

            $notification=array(
                'message'=>"Brand created Successfully",
                'alert-type'=>'success'
            );
            return Redirect()->route('brand.index')->with($notification);
        }
        catch(Exception $e){
            Log::error($e->getMessage());
            $notification=array(
                'message'=>"Brand doesn't created",
                'alert-type'=>'error'
            );

            return Redirect()->back()->with($notification);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        $brand = Brand::findOrFail($brand)->first();

        return view('admin.brand.edit')->withBrand($brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {

        try{
            $brand = Brand::findOrFail($brand)->first();

            $brand->name = $request['name'];
            $logo = $request->file('logo');
            if($logo){
                unlink($request->old_logo);
                $logo_name = date('d_m_y_i');
                $ext = strtolower($logo->getClientOriginalExtension());
                $logo_full_name = $logo_name. '.' . $ext;
                $upload_path = 'media/category/';
                $logo_url = $upload_path.$logo_full_name;
                $logo->move($upload_path, $logo_full_name);

                $brand->logo =  $logo_url;
                $brand->save();

                $notification=array(
                    'message'=>"Brand Update Successfully",
                    'alert-type'=>'success'
                );
                return redirect()->route('brand.index')->with($notification);
            }
            else{
                $brand->name = $request['name'];
                $brand->save();
                $notification=array(
                    'message'=>"Brand Updated Except Logo Successfully",
                    'alert-type'=>'success'
                );
                return redirect()->route('brand.index')->with($notification);
            }
        }
        catch(Exception $e){
            $notification=array(
                'message'=>"Brand doesn't created",
                'alert-type'=>'error'
            );

            return redirect()->back()->with($notification);

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $brand = Brand::where('id', $id)->first();
        $logo = $brand->logo;
        unlink($logo);

        $brand->delete();
        $notification=array(
            'message'=>"Brand deleted",
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);
    }

}
