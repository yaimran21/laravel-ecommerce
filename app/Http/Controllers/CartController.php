<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Cart;
use Auth;

class CartController extends Controller
{

    public function index (){
        $cart_items = Cart::content();
        $i = 0;
        return view('frontend.cart.index', compact('cart_items', 'i'));
    }

    public function add_cart ($product_id){
        $product = Product::findOrFail($product_id);
        if ($product){
            $data = array();
            $data['id'] = $product->id;
            $data['name'] = $product->title;
            $data['qty'] = 1;
            $data['price'] = $product->price;
            $data['weight'] = 20;
            $data['options'] ['image'] = $product->image;;
            $add = Cart::add($data);
        }
        return response()->json(["message"=>'Successfully added to cart', 'icon'=> 'success', 'title'=> 'Yeah!']);
//        try {
//            $data = array();
//            $data['id'] = $product->id;
//            $data['name'] = $product->name;
//            $data['qty'] = 1;
//            $data['price'] = $product->price;
//            echo $data;
////        $data['price'] = $prduct->sale_price!=null? $prduct->sale_price: $prduct->price;
////        $data['options']['image'] = '/media/products/small/'.$prduct->image;
//            $add = Cart::add($data);
//            if ($add){
//                echo 'hello';
//            }
//
//            return response()->json(["message"=> 'Successfully add your cart', 'icon'=> 'success', 'title'=> 'Yeah!']);
//        }
//        catch (\Exception $e){
//            Log::error($e->getMessage());
//            return response()->json(["message"=> 'Successfully add your cart', 'icon'=> 'error', 'title'=> 'Oh no!']);
//
//
//        }



    }
    public function check(){
        $content = Cart::content();
        return response()->json($content);
    }


    public function destroy($rowId){
        Cart::remove($rowId);
        $notification=array(
            'message'=>"Deleted card item",
            'alert-type'=>'error'
        );
        return redirect()->back()->with($notification);
    }

    public function update(Request $request){
        $rowId =  $request->rowId;
        $qty =  $request->qty;
        Cart::update($rowId, $qty);
        $notification=array(
            'message'=>"Cart Updated successfully",
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);
    }

}
