<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title class="bangla-font">বিশুদ্ধতা - @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ================= Favicon ================== -->
    <link rel="icon" sizes="72x72" href="{{asset('frontend/images/favicon-96x96.png')}}">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900%7CPoppins:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Font Awesome css-->
    <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <!-- Bootsrap css-->
    <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <!-- jquery countdown -->
    <link rel="stylesheet" href="{{asset('frontend/css/jquery.countdown.css')}}">
    <!-- REVOLUTION SLIDER STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/settings.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/navigation.css')}}">
    <!-- Animate css-->
    <link rel="stylesheet" href="{{asset('frontend/css/animate.css')}}">
    <!-- Style-->
    <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/colors/organic.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    <!-- Modernizr-->
    <script src="{{asset('frontend/js/modernizr-2.8.3.min.js')}}"></script>


    <!-- DataTables -->
    <link href="{{ asset('backend/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href=" {{ asset('backend/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href=" {{ asset('backend/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href=" {{ asset('backend/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('frontend/css/bottom_navigation.css') }}" rel="stylesheet">
    <link href="{{ url('https://fonts.maateen.me/adorsho-lipi/font.css') }}" rel="stylesheet">


    @include('frontend.__partials.bottom_nav')

</head>
<body>
@include('frontend.__partials.main_nav')
@yield('home_content')

@include('frontend.__partials.footer_content')





<!-- // End Footer  -->
<!-- == jQuery Librery == -->
<script src="{{asset('frontend/js/jquery-2.2.4.min.js')}}"></script>
<!-- == Bootsrap js File == -->
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>

<!-- == mixitup == -->
<script src="{{asset('frontend/js/mixitup.min.js')}}"></script>
<!-- == mixitup == -->
<script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
<!-- == Countdown == -->
<script src="{{asset('frontend/js/jquery.plugin.js')}}"></script>
<script src="{{asset('frontend/js/jquery.countdown.min.js')}}"></script>
<!-- == Wow js == -->
<script src="{{asset('frontend/js/wow.min.js')}}"></script>
<!-- == Bootsrap rating js File == -->
<script src="{{asset('frontend/js/bootstrap-rating.min.js')}}"></script>
<!-- == Magnific Popup == -->
<script src="{{asset('frontend/js/jquery.magnific-popup.min.js')}}"></script>
<!-- == Revolution Slider JS == -->
<script src="{{asset('frontend/js/revolution/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('frontend/js/revolution/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('frontend/js/revolution/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('frontend/js/revolution/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('frontend/js/revolution/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('frontend/js/revolution/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('frontend/js/revolution-active.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Datatables -->
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/jszip.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/pdfmake.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/vfs_fonts.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/buttons.print.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src=" {{ asset('backend/plugins/datatables/responsive.bootstrap.min.js') }}"></script>

<!-- Datatable init js -->
<script src=" {{ asset('backend/pages/jquery.datatables.init.js') }}"></script>

<!-- Data table -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable( { keys: true } );
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable( { ajax: "backend/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    TableManageButtons.init();
</script>


<!-- == custom Js File == -->
<script src="{{asset('frontend/js/custom.js')}}"></script>


<script>
    @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>
@yield('frontend_one_page_js')

</body>
</html>
