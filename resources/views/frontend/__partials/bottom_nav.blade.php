<nav class="nav">
{{--    <a href="#" class="nav__link">--}}
            <a href="#"  class=" nav__link hidden-lg hidden-md header-humberger-icon" id="category-icon">

        <i class="material-icons nav__icon">category</i>
        <span class="nav__text">category</span>
    </a>
    <a href="#" class="nav__link nav__link--active">
        <i class="fa fa-heart nav__icon"></i>
        <span class="nav__text">Wishlist</span>
    </a>
    <a href="#" class="nav__link">
        <i class="material-icons nav__icon">dashboard</i>
        <span class="nav__text">Dashboard</span>
    </a>
    <a href="#" class="nav__link">
        <i class="fa fa-shopping-cart nav__icon"></i>
        <span class="nav__text">Cart</span>
    </a>
    <a href="#" class="nav__link">
        <i class="material-icons nav__icon">person</i>
        <span class="nav__text">Profile</span>
    </a>
</nav>
