<!DOCTYPE html>
<html>

<!-- Mirrored from coderthemes.com/flacto/blue_1_light/admin-masonry.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Oct 2017 17:41:39 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <link rel="shortcut icon" href="{{ 'backend/images/favicon.ico' }}">


    <title>MPADMIN - @yield('title')</title>

    @include('admin.__partials.css_asset_link')
    @yield('on_page_css')

</head>


<body class="fixed-left">
<!-- Begin page -->
<div id="wrapper">
{{--@if(Auth::guard('admin')->check())--}}
    @include('admin.__partials.topbar')
    @include('admin.__partials.sidebar')
{{--@endif--}}

<!-- Start content -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    @yield('admin_content')
                </div> <!-- container -->
            </div> <!-- content -->
            <footer class="footer">
                2020 © ModiPonno. Design by <a href="#" target="_blank" class="text-muted">Codex</a>
            </footer>

        </div>
    @include('admin.__partials.notification')
</div>
<!-- END wrapper -->

@include('admin.__partials.js_asset_link')

</body>
</html>

