@extends('admin.admin_layouts')
@section('title', 'Category Create')
@section('admin_content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title  text-center m-t-0 m-b-30"> Add Coupon</h4>
                <form method="post" action="{{ route('coupon.store') }}">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="code">Coupon code</label>
                        <input type="text" name="code" class="form-control" id="code" placeholder="Enter unique coupon code">
                    </div>

                    <div class="form-group">
                        <label for="type"> Coupon Type</label>
                        <select id="type" class="form-control" name="type"  onchange="changeFunc();">
                            <option class="text-muted" value="" >--Select Coupon Type--</option>
                            <option value="fixed">Fixed</option>
                            <option value="percent">Percent</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input style="display: none" type="text" name="value" class="form-control"  id="fixedValue" placeholder="Enter fixed discount value">
                    </div>

                    <div class="form-group">
                        <input style="display: none" type="text" name="percent_of" class="form-control"  id="percentOf" placeholder="Enter percent value">
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status">
                            <option class="text-muted" value="" >--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </form>
            </div>
        </div><!-- end col -->
    </div>
@endsection
@section('on_page_js')

    <script>
        function changeFunc() {
            var selectBox = document.getElementById("type");
            var selectedValue = selectBox.options[selectBox.selectedIndex].value;
            if (selectedValue=="fixed"){
                $('#fixedValue').show();
                $('#percentOf').hide();
            }
            else if (selectedValue=="percent") {
                $('#percentOf').show();
                $('#fixedValue').hide();

            }
            else {
                alert("Select a valid option");
                $('#percentOf').hide();
                $('#fixedValue').hide();            }
        }
    </script>
@endsection
